//
//  main.cpp
//  DeviceSim
//
//  Created by Daniel Livingston on 9/28/15.
//  Copyright © 2015 Daniel Livingston. All rights reserved.
//

// Next steps....implement just old functions, take out function inputs like f(int ne), and 

#include <fstream>
#include <iostream>
#include <cmath>
#include <cstdlib>
#include <string>
#include <math.h>

#include "readin.hpp"
#include "scattering.hpp"
#include "poisson_SOR.hpp"
#include "device.hpp"
#include "write.hpp"

std::string s1, s2, s3, s4, s5;
std::ofstream output1, output2, output3, output4, output5;

// VERY much not sure that the following will work...
//double xx_o[nx_size+2], yy_o[ny_size+2];
//double* xx = &xx_o[nx_size+1];
//double* yy = &yy_o[ny_size+1];


int main() {

    double time, drain_factor, source_factor;
    int j, n_time_steps_av, n_times, iter_total, iter_reference;
    
    std::string path_main1 = working_dir + "charge_source_drain";
    std::ofstream file_main1(path_main1.c_str());
    
    mat_par_initialization();
    readin();
    std::cout << "material parameters initialized \n";

    device_structure_initialization();
    cout << "device structure initialized \n";

    source_drain_carrier_number();
    cout << "Number of carriers for S and D regions calculated \n";

    //     Calculate the scattering table
    scat_table(); // change this
    cout << "Scattering table calculated \n";
    
    // Temporary substitute for reading the above files
    V_source_input = 0.;
    V_drain_input = 0.8;
    V_gate_input = 1.2;
    V_substrate_input = 0.;
    ax = 10.e-10;
    ay = 5.e-10;


    //     Solve equilibrium Poisson equation
    V_source = 0.;
    V_drain = 0.;
    V_gate = V_gate_input;
    V_substrate = 0.;
    V_gate = V_gate/Vt;

    apply_voltage();
    poisson_SOR(0.);
    //quantum_correction(ax,ay)
    electric_field_update();

    //     Initialize electrons
    electrons_initialization();
    count_used_particles(ne);
    printf("Number of electrons in use = %d \n", ne);

    write_mesh();
    write_electron_distribution();
    write_electric_field();
    write_potential(0.);
    printf("Vt0: %f \n", Vt);

    //     Apply bias at the contacts
    V_source = V_source_input;
    V_drain = V_drain_input;
    V_substrate = V_substrate_input;
    V_source = V_source/Vt;
    V_drain = V_drain/Vt;
    V_substrate = V_substrate/Vt;
    apply_voltage();
    
    //     Start the Monte Carlo procedure
    time = 0.;
    j = 0;
    n_time_steps_av = int(averaging_time/dt);
    n_times = int(tot_time/dt)/n_time_steps_av;
    iter_total = n_time_steps_av*n_times;
    printf("Number of time steps for averaging = %d \n", n_time_steps_av);
    printf("Total number of iterations = %d \n", iter_total);

    while (j <= iter_total) {
    
        j = j + 1;
        
        //file_main1 << j << std::endl;
        
        time = dt*float(j);
    
        free_flight_scatter();
    //          call count_used_particles(ne)
    
        check_source_drain_contacts();
    //          call count_used_particles(ne)
    //          print*,'Number of electrons in use = ',ne
    
        delete_particles();
        charge_assignment_NEC();
        poisson_SOR(1.);
    //          call quantum_correction(ax,ay)
    //          print*,'quantum corr:',iter_total+1-j
        electric_field_update();
        iter_reference = (j % n_time_steps_av);
    
    if (iter_reference == 0) {
        std::cout << "time = " << time << std::endl;
        count_used_particles(ne);
        cout << time << " " << idd_out << " " << idd_eli << " " << idd_cre << std::endl;
        cout << time << " " << iss_out << " " << iss_eli << " " << iss_cre << std::endl;
        cout << "Number of electrons in use = " << ne << std::endl;
        
        write_electron_distribution();
        write_electric_field();
        write_potential(1.);
        }
        
        velocity_energy_cumulative(iter_reference, n_time_steps_av);
        drain_factor = float(idd_out + idd_eli - idd_cre);
        source_factor = float(iss_out + iss_eli - iss_cre);
        
        file_main1 << time << " " << source_factor << " " << drain_factor << std::endl;
        
        }	// End of the time loop
    
    file_main1.close();
    
lbl_main_222:
    cout << "Simulation ended. \n";
    
    return 0;
    
    }
