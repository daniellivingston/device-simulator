//
//  write.cpp
//  DeviceSim
//
//  Created by Daniel Livingston on 10/5/15.
//  Copyright © 2015 Daniel Livingston. All rights reserved.
//

#include <fstream>

#include "write.hpp"
#include "readin.hpp"

int write_mesh() {
    
    int i, j;
    
    std::string path_x = working_dir + "x_axis";
    std::string path_y = working_dir + "y_axis";
    std::ofstream x_file(path_x.c_str());
    std::ofstream y_file(path_y.c_str());
    
    //     Write x-axis
    
    for (i = 0; i < nx_max; i++) {
        x_file << x_position << std::endl;
        x_position = x_position + xx[i] * debye_length;
    }
    
    x_file.close();
    
    //     Write y-axis
    
    for (j = 0; j < ny_max; j++) {
        y_file << y_position << std::endl;
        y_position = y_position + yy[j] * debye_length;
    }
    
    y_file.close();
    
    return 0;
}


int write_electron_distribution() {
    
    int i, j, n;
    
    for (i = 0; i < nx_max; i++) {
        for (j = 0; j < ny_max; j++) {
            elec_number[i][j] = 0;
        }
    }
    
    for (n = 1; n < n_used; n++) {
        if(ip[n] != 9) {
            
            i = int(p[n][5]/mesh_sizex+0.5);
            j = int(p[n][6]/mesh_sizey+0.5);
            if(i > nx_max) { i = nx_max; }
            
            if(i < 0) {
                cout << "found negative coordinate along x \n";
                cout << "x-position = " << x_position << "\n";
                i = 0;
            }
            
            if(j > ny_max) { j = ny_max; }
            if(j < 0) { j = 0; }
            
            if(j < 0) {
                cout << "found negative coordinate along y \n";
                cout << "y-position = " << y_position << "\n";
                j = 0;
            }
            
            elec_number[i][j] = elec_number[i][j] + 1;
            
        }
    }
    
    
    std::string path_ed = working_dir + "electron_distribution";
    std::ofstream ed_file(path_ed.c_str());
    
    for (i = 0; i < nx_max; i++) {
        for (j = 0; j < ny_max; j++) {
            ed_file << elec_number[i][j] << std::endl;
        }
    }
    
    ed_file.close();
    
    
    return 0;
}

int write_electric_field() {
    
    int i, j;
    
    std::string path_fx = working_dir + "electric_field_x_component";
    std::string path_fy = working_dir + "electric_field_y_component";
    std::ofstream fx_file(path_fx.c_str());
    std::ofstream fy_file(path_fy.c_str());
    
    
    for (i = 0; i < nx_max; i++) {
        for (j = 0; j < ny_max; j++) {
            fx_file << fx_field[i][j] << std::endl;
        }
    }
    fx_file.close();
    
    
    for (i = 0; i < nx_max; i++) {
        for (j = 0; j < ny_max; j++) {
            fy_file << fy_field[i][j] << std::endl;
        }
    }
    fy_file.close();
    
    return 0;
}

int write_potential(int flag_solution) {
    
    int i, j;
    double ro;
    
    std::string path_wp1 = working_dir + "potential";
    std::string path_wp2 = working_dir + "electron_density";
    std::string path_wp3 = working_dir + "hole_density";
    std::string path_wp4 = working_dir + "charge_density";
    std::string path_wp5 = working_dir + "doping";
    std::ofstream file_wp1(path_wp1.c_str());
    std::ofstream file_wp2(path_wp2.c_str());
    std::ofstream file_wp3(path_wp3.c_str());
    std::ofstream file_wp4(path_wp4.c_str());
    std::ofstream file_wp5(path_wp5.c_str());
    
    for (i = 0; i < nx_max; i++) {
        for (j = 0; j < ny_max; j++) {
            
            if (flag_solution == 0.) {
                denn = exp(fai2[i][j]);
            } else if(flag_solution == 1.) {
                denn = electron_density[i][j];
            }
            
            denn_inv = exp(-fai2[i][j]);
            ro = denn_inv - denn + doping[i][j];
            file_wp1 << ((delta_Ec - fai2[i][j])*Vt) << std::endl;
            file_wp2 << (denn*intrinsic_carrier_density) << std::endl;
            file_wp3 << (denn_inv*intrinsic_carrier_density) << std::endl;
            file_wp4 << (ro*intrinsic_carrier_density) << std::endl;
            file_wp5 << (doping[i][j]*intrinsic_carrier_density) << std::endl;
            
        }
    }
    
    file_wp1.close();
    file_wp2.close();
    file_wp3.close();
    file_wp4.close();
    file_wp5.close();
    
    return 0;
}


int velocity_energy_cumulative(int iter_reference, int n_time_steps_av) {
    
    int n_valley = 3;
    
    int i, j, n;
    double velx, vely, denom, term;
    double velx_sum[nx_size+1][n_valley+1], vely_sum[nx_size+1][n_valley+1], nvaly[nx_size+1][n_valley], sume[nx_size+1][n_valley], el_number[nx_size+1];
    
    velx = 0;
    vely = 0;
    
    if(iter_reference == 0) {
        
        std::string path_vec1 = working_dir + "velocity_energy_averages";
        std::string path_vec2 = working_dir + "current_averages";
        std::ofstream file_vec1(path_vec1.c_str());
        std::ofstream file_vec2(path_vec2.c_str());
        
        double vx_s, vy_s, es_s;
        
        for (i = 0; i < nx_max; i++) {
            vx_s = velocity_x_sum[i]/float(n_time_steps_av);
            vy_s = velocity_y_sum[i]/float(n_time_steps_av);
            es_s = energy_sum[i]/float(n_time_steps_av);
            
            file_vec1 << vx_s << " " << vy_s << " " << es_s << std::endl;
        }
        
        file_vec1.close();
        
        for (i = 0; i < nx_max; i++) {
            file_vec2 << (current_sum[i]/float(n_time_steps_av)) << std::endl;
        }
        
        file_vec2.close();
        
        for (i = 0; i < nx_max; i++) {
            velocity_x_sum[i] = 0.;
            velocity_y_sum[i] = 0.;
            velocity_z_sum[i] = 0.;
            energy_sum[i] = 0.;
            current_sum[i] = 0.;
        }
        
    }
    
    for (i = 0; i < nx_max; i++) {
        el_number[i] = 0;
        for (j = 1; j < n_valley; j++) {
            nvaly[i][j] = 0;
            velx_sum[i][j] = 0.;
            vely_sum[i][j] = 0.;
            sume[i][j] = 0.;
        }
    }
    
    for (n = 1; n < n_used; n++) {
        
        if (ip[n] != 9) {
            
            i = int(p[n][5]/mesh_sizex+0.5);
            
            if(i < 0) { i = 0; }
            
            if(i > nx_max) { i = nx_max; }
            iv = ip[n];
            
            if(iv > n_valley) { iv = n_valley; }
            
            ee = energy[n];
            denom = 1./(1.+af2[iv]*ee);
            
            if (iv == 1) {
                velx = hm[1] * p[n][1] * denom;
                vely = hm[2] * p[n][2] * denom;
                //           velz = hm[3] * p[n][3] * denom;
            }
            if (iv == 2) {
                velx = hm[2] * p[n][1] * denom;
                vely = hm[1] * p[n][2] * denom;
                //             velz = hm[3] * p[n][3]*denom;
            }
            
            if(iv == 3) {
                velx = hm[3] * p[n][1] * denom;
                vely = hm[2] * p[n][2] * denom;
                //             velz = hm[1] * p[n][3] * denom;
            }
            
            //         print *,i,iv
            velx_sum[i][iv] = velx_sum[i][iv] + velx;
            vely_sum[i][iv] = vely_sum[i][iv] + vely;
            //          velz_sum[i][iv] = velz_sum[i][iv] + velz;
            sume[i][iv] = sume[i][iv] + ee;
            nvaly[i][iv] = nvaly[i][iv] + 1;
            el_number[i] = el_number[i] + 1;
            
        }
    }
    
    for (i = 0; i < nx_max; i++) {
        
        if (el_number[i] != 0) {
            
            factor = 1./el_number[i];
            term = velx_sum[i][1] + velx_sum[i][2] + velx_sum[i][3];
            velocity_x_sum[i] = velocity_x_sum[i] + term*factor;
            current_sum[i] = current_sum[i] + term*q/mesh_sizex;
            term = vely_sum[i][1] + vely_sum[i][2] + vely_sum[i][3];
            velocity_y_sum[i] = velocity_y_sum[i] + term*factor;
            term = (sume[i][1] + sume[i][2] + sume[i][3])*factor;
            energy_sum[i] = energy_sum[i] + term;
            
        }
        
    }
    
    return 0;
}

