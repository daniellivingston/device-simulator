//
//  readin.hpp
//  DeviceSim
//
//  Created by Daniel Livingston on 10/5/15.
//  Copyright © 2015 Daniel Livingston. All rights reserved.
//

#ifndef readin_hpp
#define readin_hpp

#include <stdio.h>
#include <math.h>
#include <string>
#include <iostream>

#define sanity 0 // Perform sanity checks?

#define am0 9.11e-31
#define h 1.05459e-34
#define q 1.60219e-19
#define eps 8.85419e-12
#define kb 1.38055e-23
#define pi 3.14159265
#define carriers 5000

#define n_carriers 1000
#define n_scatt_max 10
#define n_doping_regions 5
#define max_electron_number 30000
#define averaging_time 10.e-14
#define nx_size 75
#define ny_size 200

using namespace std;

// Change the below working directory to match yours
//std::string working_dir;

extern std::string working_dir;
extern std::string s1, s2, s3, s4, s5;
extern std::ofstream output1, output2, output3, output4, output5;

extern int n_lev;

extern int sanity_cycle, sanity_max;
extern double elec_min, elec_step;

extern int nele, nsim, iter;
extern int iv, i_count, i_region;

extern int final_valleys, i_final;
extern double sigma, w0, coupling_constant, delta_fi;

extern int iso;
extern double qh, eps_0, two_pi;

extern double dt, tot_time;
extern double tem, Vt;
extern double fx, fy, fz;

extern double am_l, am_t, amd, amc;
extern double nonparabolicity_factor;

extern double rel_mass_gamma, rel_mass_L, rel_mass_X;
extern double nonparabolicity_gamma, nonparabolicity_L, nonparabolicity_X;
extern double split_L_gamma, split_X_gamma;
extern int eq_valleys_gamma, eq_valleys_L, eq_valleys_X;

extern double eps_high, eps_low;
extern double density, sound_velocity;
extern double emax, ee, fe, de;

extern bool Coulomb_scattering;
extern bool acoustic_gamma, acoustic_L, acoustic_X;
extern bool polar_gamma, polar_L, polar_X;
extern bool intervalley_gamma_L, intervalley_gamma_X, intervalley_L_gamma, intervalley_L_L;
extern bool intervalley_L_X, intervalley_X_gamma, intervalley_X_L, intervalley_X_X;

extern double DefPot_zero_g, DefPot_zero_f, DefPot_first_g, DefPot_first_f;
extern double sigma_acoustic, phonon_zero_g, phonon_zero_f, phonon_first_g, phonon_first_f;

extern double sigma_gamma, sigma_L, sigma_X;
extern double polar_en_gamma, polar_en_L, polar_en_X;
extern double phonon_gamma_L, phonon_gamma_X, phonon_L_gamma, phonon_L_L, phonon_L_X, phonon_X_gamma, phonon_X_L, phonon_X_X;
extern double DefPot_gamma_L, DefPot_L_gamma, DefPot_gamma_X, DefPot_X_gamma, DefPot_L_L, DefPot_X_X, DefPot_L_X, DefPot_X_L;

//  double Energy_debye;
extern double kx, ky, kz, e;
extern double k, kxy, kxp, kyp, kzp, kp;

extern double am[3+1], hm[3+1], tm2[3+1];

extern double smh[3+1], hhm[3+1], freq[10+1][3+1];
extern double af[3+1], af2[3+1], af4[3+1];
extern double w[10+1][3+1], tau_max[3+1], max_scatt_mech[3+1];
extern double flag_mech[10+1][3+1], i_valley[10+1][3+1];
extern double scatt_table[4000+1][10+1][3+1];
extern double p[max_electron_number+1][8+1], ip[max_electron_number+1], energy[max_electron_number+1];

//////////////////////////////////////////////////////////////////

extern double V_source, V_drain, V_gate, V_gate_input, V_substrate;
extern double V_source_input, V_drain_input, V_substrate_input, ax, ay;

extern double eps_sc, eps_oxide, gamma_c, intrinsic_carrier_density, delta_Ec, debye_length;

extern double device_le;
extern bool acoustic_scattering, intervalley_zero_g, intervalley_zero_f;

// Device Parameters ////////////////////////////////////////////////////

extern int n_region, np_ij, ne, n_used;
extern int n_s, n_d, n_j, nx_max, ny_max;
extern int ix_fix, jy_fix;

extern int elec_number[nx_size+1][ny_size+1];
extern int npt[nx_size+1], ix, jy, n_number;

extern float idd_out, iss_out, idd_eli, iss_eli, idd_cre, iss_cre;

extern double x_position, y_position, factor;
extern double source_drain_length, source_drain_depth;
extern double gate_length, gate_width, bulk_depth, device_width, device_depth, device_length;
extern double mesh_sizex, mesh_sizey, inv_mesh_sizex, inv_mesh_sizey, omega_c, tolerance, oxide_thickness;

extern bool flag_conv;
extern double forcing_func[nx_size+1][ny_size+1], c2[nx_size+1][ny_size+1];
extern double electron_density[nx_size+1][ny_size+1];
extern double beta[nx_size+1];

extern double dop_term, denn, denn_inv;

extern double velocity_x_sum[nx_size+1], velocity_y_sum[nx_size+1], velocity_z_sum[nx_size+1], energy_sum[nx_size+1], current_sum[nx_size+1];
extern int flag_dom[nx_size+1][ny_size+1];
extern int ix_min[n_doping_regions+1], ix_max[n_doping_regions+1], jy_min[n_doping_regions+1], jy_max[n_doping_regions+1];
extern int nsource_drain_carriers[nx_size+1];
extern double fai[nx_size+1][ny_size+1], fai2[nx_size+1][ny_size+1], doping[nx_size+1][ny_size+1];
//  double doping_density, Energy_debye;
extern double doping_density[n_doping_regions+1], Energy_debye[n_doping_regions+1]; // called as X[i_region] in Si code (Coulomb_BH)
extern double fx_field[nx_size+1][ny_size+1], fy_field[nx_size+1][ny_size+1];

extern double a_coeff[nx_size+1][ny_size+1], b_coeff[nx_size+1][ny_size+1], c_coeff[nx_size+1][ny_size+1], d_coeff[nx_size+1][ny_size+1], e_coeff[nx_size+1][ny_size+1], alpha[nx_size+1][ny_size+1];

extern double xx[nx_size+2], yy[ny_size+2];

int readin();
int mat_par_initialization();

#endif /* readin_hpp */
