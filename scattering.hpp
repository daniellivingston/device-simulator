//
//  scattering.hpp
//  DeviceSim
//
//  Created by Daniel Livingston on 10/5/15.
//  Copyright © 2015 Daniel Livingston. All rights reserved.
//

#ifndef scattering_hpp
#define scattering_hpp

#include <stdio.h>
#include <math.h>
#include <cstdlib>
#include "readin.hpp"

int drift(double tau);
int isotropic(int i_fix);
int isotropic_g(int i_fix);
int isotropic_f(int i_fix);
int polar_optical_angle(int i_fix);
int Coulomb_angle_BH();
int scatter_carrier();
int free_flight_scatter();
int free_flight_scatter_new();
int histograms(std::string file_name);
int write_m(int nsim, int iter, double time, bool flag_write);
int acoustic_rate_2(std::string out_file, std::string out_file_2);
int acoustic_rate(std::string out_file, std::string out_file_2);
int Coulomb_BH(std::string out_file);
int polar_rate(std::string out_file_1, std::string out_file_2);
int intervalley(double w0, std::string out_file_1, std::string out_file_2);
int renormalize_table();
int scat_table();

#endif /* scattering_hpp */
