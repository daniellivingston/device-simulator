//
//  readin.cpp
//  DeviceSim
//
//  Created by Daniel Livingston on 10/5/15.
//  Copyright © 2015 Daniel Livingston. All rights reserved.
//

#include "readin.hpp"
#include <fstream>
#include <math.h>

using namespace std;

std::string working_dir = "/Users/daniellivingston/MC_OUTPUT/DEVICE_SIM/";

int n_lev = n_carriers;

// Change the below working directory to match yours

int sanity_cycle, sanity_max;
double elec_min, elec_step;

int nele, nsim, iter;
int iv, i_count, i_region;

int final_valleys, i_final;
double sigma, w0, coupling_constant, delta_fi;

int iso;
double qh, eps_0, two_pi;

double dt, tot_time;
double tem, Vt;
double fx, fy, fz;

double am_l, am_t, amd, amc;
double nonparabolicity_factor;

double rel_mass_gamma, rel_mass_L, rel_mass_X;
double nonparabolicity_gamma, nonparabolicity_L, nonparabolicity_X;
double split_L_gamma, split_X_gamma;
int eq_valleys_gamma, eq_valleys_L, eq_valleys_X;

double eps_high, eps_low;
double density, sound_velocity;
double emax, ee, fe, de;

bool Coulomb_scattering;
bool acoustic_gamma, acoustic_L, acoustic_X;
bool polar_gamma, polar_L, polar_X;
bool intervalley_gamma_L, intervalley_gamma_X, intervalley_L_gamma, intervalley_L_L;
bool intervalley_L_X, intervalley_X_gamma, intervalley_X_L, intervalley_X_X;

double DefPot_zero_g, DefPot_zero_f, DefPot_first_g, DefPot_first_f;
double sigma_acoustic, phonon_zero_g, phonon_zero_f, phonon_first_g, phonon_first_f;

double sigma_gamma, sigma_L, sigma_X;
double polar_en_gamma, polar_en_L, polar_en_X;
double phonon_gamma_L, phonon_gamma_X, phonon_L_gamma, phonon_L_L, phonon_L_X, phonon_X_gamma, phonon_X_L, phonon_X_X;
double DefPot_gamma_L, DefPot_L_gamma, DefPot_gamma_X, DefPot_X_gamma, DefPot_L_L, DefPot_X_X, DefPot_L_X, DefPot_X_L;

//  double Energy_debye;
double kx, ky, kz, e;
double k, kxy, kxp, kyp, kzp, kp;

double am[3+1], hm[3+1], tm2[3+1];

double smh[3+1], hhm[3+1], freq[10+1][3+1];
double af[3+1], af2[3+1], af4[3+1];
double w[10+1][3+1], tau_max[3+1], max_scatt_mech[3+1];
double flag_mech[10+1][3+1], i_valley[10+1][3+1];
double scatt_table[4000+1][10+1][3+1];
double p[max_electron_number+1][8+1], ip[max_electron_number+1], energy[max_electron_number+1];

//////////////////////////////////////////////////////////////////

double V_source, V_drain, V_gate, V_gate_input, V_substrate;
double V_source_input, V_drain_input, V_substrate_input, ax, ay;

double eps_sc, eps_oxide, gamma_c, intrinsic_carrier_density, delta_Ec, debye_length;

double device_le;
bool acoustic_scattering, intervalley_zero_g, intervalley_zero_f;

// Device Parameters ////////////////////////////////////////////////////

int n_region, np_ij, ne, n_used;
int n_s, n_d, n_j, nx_max, ny_max;
int ix_fix, jy_fix;

int elec_number[nx_size+1][ny_size+1];
int npt[nx_size+1], ix, jy, n_number;

float idd_out, iss_out, idd_eli, iss_eli, idd_cre, iss_cre;

double x_position, y_position, factor;
double source_drain_length, source_drain_depth;
double gate_length, gate_width, bulk_depth, device_width, device_depth, device_length;
double mesh_sizex, mesh_sizey, inv_mesh_sizex, inv_mesh_sizey, omega_c, tolerance, oxide_thickness;

bool flag_conv;
double forcing_func[nx_size+1][ny_size+1], c2[nx_size+1][ny_size+1];
double electron_density[nx_size+1][ny_size+1];
double beta[nx_size+1];

double dop_term, denn, denn_inv;

double velocity_x_sum[nx_size+1], velocity_y_sum[nx_size+1], velocity_z_sum[nx_size+1], energy_sum[nx_size+1], current_sum[nx_size+1];
int flag_dom[nx_size+1][ny_size+1];
int ix_min[n_doping_regions+1], ix_max[n_doping_regions+1], jy_min[n_doping_regions+1], jy_max[n_doping_regions+1];
int nsource_drain_carriers[nx_size+1];
double fai[nx_size+1][ny_size+1], fai2[nx_size+1][ny_size+1], doping[nx_size+1][ny_size+1];
//  double doping_density, Energy_debye;
double doping_density[n_doping_regions+1], Energy_debye[n_doping_regions+1]; // called as X[i_region] in Si code (Coulomb_BH)
double fx_field[nx_size+1][ny_size+1], fy_field[nx_size+1][ny_size+1];

double a_coeff[nx_size+1][ny_size+1], b_coeff[nx_size+1][ny_size+1], c_coeff[nx_size+1][ny_size+1], d_coeff[nx_size+1][ny_size+1], e_coeff[nx_size+1][ny_size+1], alpha[nx_size+1][ny_size+1];

double xx[nx_size+2], yy[ny_size+2];

////////////////////////////////////

int readin() {
    
    
    ////////    MATERIAL PARAMETERS  ////////////////
    
    // Define masses for different valleys
    
    rel_mass_gamma = 0.063;
    rel_mass_L = 0.170;
    rel_mass_X = 0.58;
    
    // Define longitudinal and transverse masses
    //      Source: Ioffe
    
    am_l = 1.59; // XXXX
    am_t = 0.0815; // XXXX
    
    // Define GaAs-based non-parabolicity factors
    
    nonparabolicity_gamma = 0.62;
    nonparabolicity_L = 0.50;
    nonparabolicity_X = 0.30;
    
    // Define Si-based non-parabolicity factor
    
    nonparabolicity_factor = 0.5;
    
    // Define valley splitting and equivalent valleys
    
    split_L_gamma = -0.14;//0.29; See paper
    split_X_gamma = -0.05;//0.48; See paper (Vasileska)
    eq_valleys_gamma = 1; // Is this the case for Ge?
    eq_valleys_L = 4;
    eq_valleys_X = 3;
    
    // Dielectric constant for Ge
    //      Source: Ioffe
    
    eps_low = 16.2*eps_0; // XXXX
    eps_high = eps_low; // XXXX
    
    // Define crystal density and sound velocity
    //      Source: Aubrey-Fortuna
    
    density = 5327.; // XXXX
    sound_velocity = 5.4E3; // XXXX
    
    // Define coupling constants - Ge (eV)
    //      Source: Aubry-Fortuna
    
    sigma_gamma = 5.0; // For <000>?
    sigma_L = 11.0; // For <111>?
    sigma_X = 9.0; // For <100>?
    
    polar_en_gamma = 0.037; // XXXX
    polar_en_L = 0.037; // XXXX
    polar_en_X = 0.037; // XXXX
    
    phonon_gamma_L = 0.0276; // XXXX
    phonon_L_gamma = 0.0276; // XXXX
    phonon_L_X = 0.0276; // XXXX
    phonon_X_L = 0.0276; // XXXX
    phonon_X_gamma = 0.0276; // XXXX
    phonon_gamma_X = 0.0276; // XXXX
    phonon_L_L = 0.02756; // XXXX*
    phonon_X_X = 0.03704; // XXXX*
    
    // Define force constants - Ge (eV/m)
    //      Source: Aubry-Fortuna
    
    DefPot_gamma_L = 2.0E10; // XXXX
    DefPot_L_gamma = 2.0E10; // XXXX
    DefPot_gamma_X = 1.0E11; // XXXX
    DefPot_X_gamma = 1.0E11; // XXXX
    DefPot_L_L = 3.0E10; // XXXX*
    DefPot_X_X = 9.46E10; // XXXX
    DefPot_L_X = 4.06E10; // XXXX
    DefPot_X_L = 4.06E10; // XXXX
    
    Coulomb_scattering = 0;//1;
    acoustic_gamma = 1;
    acoustic_L = 1;
    acoustic_X = 1;
    polar_gamma = 1;
    polar_L = 1;
    polar_X = 1;
    intervalley_gamma_L = 1;
    intervalley_gamma_X = 1;
    intervalley_L_gamma = 1;
    intervalley_L_L = 1;
    intervalley_L_X = 1;
    intervalley_X_gamma = 1;
    intervalley_X_L = 1;
    intervalley_X_X = 1;
    
    int i = 0;
    
    // Constant declaration for Si basis
    
    amd = am0*pow((am_l*am_t*am_t),(1.0/3.0));
    amc = am0*3.0/(1.0/am_l + 2.0/am_t);
    
    tm2[1] = sqrt(amc/am_l);
    tm2[2] = sqrt(amc/am_t);
    tm2[3] = sqrt(amc/am_t);
    
    hm[1] = h/amc*tm2[1];
    hm[2] = h/amc*tm2[2];
    hm[3] = h/amc*tm2[3];
    
    am[1] = amc;
    am[2] = amc;
    am[3] = amc;
    af[1] = nonparabolicity_factor;
    af[2] = nonparabolicity_factor;
    af[3] = nonparabolicity_factor;
    
    af2[i] = 2.*af[i];
    af4[i] = 4.*af[i];
    
    for (i = 1; i <= 4; i++) {
        smh[i] = sqrt(2.*amc)*sqrt(q)/h;
        hhm[i] = h/amc/q*h/2.;
        af2[i] = 2.*af[i];
        af4[i] = 4.*af[i];
    }
    
    return 0;
}

int mat_par_initialization() {
    
    // if (sanity == 1) {
    n_lev = carriers;
    nele = carriers;
    iv = 0;
    i_count = 0;
    
    dt = 1.e-14;
    tot_time = 100.e-12;
    
    tem = 300.0;
    Vt = kb*tem/q;
    //      doping_density = 1.e21;
    
    fx = 0;
    fy = elec_min + sanity_cycle*elec_step;
    fz = 0;
    
    emax = 1;
    de = emax/((float)(n_lev));
    
    int i = 0;
    
    // Constant declaration for Si basis
    
    amd = am0*pow((am_l*am_t*am_t),(1.0/3.0));
    amc = am0*3.0/(1.0/am_l + 2.0/am_t);
    
    tm2[1] = sqrt(amc/am_l);
    tm2[2] = sqrt(amc/am_t);
    tm2[3] = sqrt(amc/am_t);
    
    hm[1] = h/amc*tm2[1];
    hm[2] = h/amc*tm2[2];
    hm[3] = h/amc*tm2[3];
    
    am[1] = amc;
    am[2] = amc;
    am[3] = amc;
    af[1] = nonparabolicity_factor;
    af[2] = nonparabolicity_factor;
    af[3] = nonparabolicity_factor;
    
    af2[i] = 2.*af[i];
    af4[i] = 4.*af[i];
    
    for (i = 1; i <= 4; i++) {
        smh[i] = sqrt(2.*amc)*sqrt(q)/h;
        hhm[i] = h/amc/q*h/2.;
        af2[i] = 2.*af[i];
        af4[i] = 4.*af[i];
    }
    
    iso = 1345;
    //  am0 = 9.11e-31;
    // h = 1.05459e-34;
    //q = 1.60219e-19;
    qh = q/h;
    eps_0 = 8.85419e-12;
    //kb = 1.38066e-23;
    //pi = 4.*atan(1.0);
    two_pi = 2.*pi;
    
    //      Define time step and maximum simulation time
    
    dt = 0.1e-15;
    tot_time = 4.0e-12;
    
    //      Set temperature
    
    tem = 300.;
    Vt = kb*tem/q;
    cout << "Thermal voltage = " << Vt << std::endl;
    
    //      Define material parameters for Si and SiO2
    
    am_l = 0.91;
    am_t = 0.19;
    nonparabolicity_factor = 0.5;
    eps_sc = 11.8;
    eps_sc = eps_sc*eps_0;
    eps_oxide = 3.9;
    eps_oxide = eps_oxide*eps_0;
    gamma_c = eps_oxide/eps_sc;
    density = 2329.;
    sound_velocity = 9040.;
    intrinsic_carrier_density= 1.45e16;
    delta_Ec = 0.575/Vt;
    debye_length = sqrt(eps_sc*Vt/q/intrinsic_carrier_density);
    
    //      Define parameters for the scattering table
    
    emax = 1.0;
    de = emax/float(n_lev);
    
    //      Coulomb_scattering = 1;
    acoustic_scattering = 1;
    intervalley_zero_g = 1;
    intervalley_zero_f = 1;
    //      intervalley_first_g = 1;
    //      intervalley_first_f = 1;
    
    sigma_acoustic = 6.55;  //  [eV]
    DefPot_zero_g = 5.23E10;   // [eV/m]
    DefPot_zero_f = 5.23E10;   // [eV/m]
    DefPot_first_g = 0.E10;   // [eV/m]
    DefPot_first_f = 0.E10;   // [eV/m]
    
    phonon_zero_g = 0.063;  //  [eV]
    phonon_zero_f = 0.059;  //  [eV]
    phonon_first_g = 0.0278;  //  [eV]
    phonon_first_f = 0.029;  //  [eV]
    
    // Device Parameters ////////////////////////
    
    source_drain_length = 50.e-9;
    source_drain_depth = 36.e-9;
    gate_length = 50.e-9;
    bulk_depth = 64.e-9;
    device_width = 0.3e-6;
    oxide_thickness = 2.e-9;
    mesh_sizex = 2.0e-9;
    mesh_sizey = 0.5e-9;
    n_region = 4;
    
    // Enable doping density again once code compiles
    doping_density[1] = 1.e25;
    doping_density[2] = 1.e25;
    doping_density[3] = -1.e24;
    doping_density[4] = -1.e24;
    omega_c = 1.8;
    tolerance = 1.e-5;
    
    return 0;
}
