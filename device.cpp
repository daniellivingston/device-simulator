//
//  device.cpp
//  DeviceSim
//
//  Created by Daniel Livingston on 10/5/15.
//  Copyright © 2015 Daniel Livingston. All rights reserved.
//

#include "device.hpp"
#include "readin.hpp"
#include "poisson_SOR.hpp"



int device_structure_initialization() {
    
    tolerance = tolerance/Vt;
    oxide_thickness = oxide_thickness/debye_length;
    
    //     Specify reference points for source and drain regions and junction depth
    
    n_s = int(source_drain_length/mesh_sizex);
    n_d = int((source_drain_length+gate_length)/mesh_sizex);
    n_j = int(source_drain_depth/mesh_sizey);
    printf("n_s, nd: %d , %d \n", n_s, n_d);
    
    nx_max = int((2.*source_drain_length+gate_length)/mesh_sizex+0.5);
    
    if(nx_max > nx_size) {
        printf("x-mesh size exceeds maximum size \n");
        std::exit;
    }
    
    device_length = float(nx_max)*mesh_sizex;
    
    ny_max = int((source_drain_depth+bulk_depth)/mesh_sizey+0.5);
    
    if(ny_max > ny_size) {
        printf("y-mesh size exceeds maximum size \n");
        std::exit;
    }
    
    device_depth = float(ny_max)*mesh_sizey;
    
    printf("Mesh size based on device dimensions: \n");
    printf("nx_max = %d , ny_max = %d \n \n", nx_max, ny_max);
    
    //     Specify flags for Poisson equation solution
    
    int i, j;
    
    //     (a) bulk
    for (i = 0; i < nx_max; i++) {
        for (j = 0; j < ny_max; j++) {
            flag_dom[i][j] = 3;
        }
    }
    
    //     (b) ohmic contacts
    j = 0;
    for (i = 0; i < n_s; i++) {
        flag_dom[i][j] = 1;
    }
    
    for (i = n_d; i < nx_max; i++) {
        flag_dom[i][j] = 1;
    }
    
    j = ny_max;
    
    for (i = 0; i < nx_max; i++) {
        flag_dom[i][j] = 1;
    }
    
    //     (c) semiconductor-oxide interface
    j = 0;
    
    for (i = n_s+1; i < n_d-1; i++) {
        flag_dom[i][j] = 2;
    }
    
    //     Define doping regions
    
    domain_boundaries(1, 0, n_s, 0, n_j);
    domain_boundaries(2, n_d, nx_max, 0, n_j);
    domain_boundaries(3, n_s+1, n_d-1, 0,n_j);
    domain_boundaries(4, 0, nx_max, n_j+1, ny_max);
    
    //     Specify doping and initial potential in various regions
    
    initialize_doping_potential();
    printf("doping and potential initialized \n \n");
    
    //     Initialize mesh
    
    mesh_initialization();
    printf("mesh initialized \n \n");
    
    //     Calculate discretization coefficients for the Poisson equation
    
    coefficients_poisson();
    cout << "poisson discretization coefficients calculated" << std::endl;
    
    return 0;
}

int source_drain_carrier_number() {
    
    int i, n_number;
    double cell_volume = 0.5*mesh_sizex*mesh_sizey*device_width;
    
    int j = 0;
    
    for (i = 0; i < nx_max; i++) {
        denn = doping[i][j] * intrinsic_carrier_density;
        if((i == 0) || (i == nx_max)) { denn = 0.5*denn; }
        factor = denn*cell_volume;
        n_number = int(factor);
        //         error = factor - float(n_number)
        //         if(ran(iso).le.error)then
        //            n_number = n_number + 1
        //         endif
        nsource_drain_carriers[i] = n_number;
    }
    
    return 0;
    
}

int apply_voltage() {
    
    //     Source region
    int i, j, n;
    
    j = 0;
    
    for (i = 0; i < n_s; i++) {
        n = int(find_region(i,j));
        dop_term = doping_density[n]/intrinsic_carrier_density;
        if(dop_term > 0) {
            factor = dop_term + sqrt(pow(dop_term,2.0)+1.);
            fai[i][j] = log(dop_term);
        } else if(dop_term <= 0) {
            dop_term = - dop_term;
            factor = dop_term + sqrt(pow(dop_term,2.0)+1.);
            fai[i][j] = - log(dop_term);
        }
        
        fai[i][j] = fai[i][j] + V_source;
    }
    
    //     Drain region
    
    j = 0;
    for (i = n_d; i < nx_max; i++) {
        n = int(find_region(i,j));
        dop_term = doping_density[n]/intrinsic_carrier_density;
        if(dop_term > 0) {
            factor = dop_term + sqrt(pow(dop_term,2.0)+1.);
            fai[i][j] = log(dop_term);
        } else if(dop_term <= 0) {
            dop_term = -dop_term;
            factor = dop_term + sqrt(pow(dop_term,2.0)+1.);
            fai[i][j] = -log(dop_term);
        }
        
        fai[i][j] = fai[i][j] + V_drain;
    }
    
    //     Substrate
    j = ny_max;
    for (i = 0; i < nx_max; i++) {
        n = int(find_region(i,j));
        dop_term = doping_density[n]/intrinsic_carrier_density;
        if(dop_term > 0) {
            factor = dop_term + sqrt(pow(dop_term,2.0)+1.);
            fai[i][j] = log(dop_term);
        } else if(dop_term <= 0) {
            dop_term = - dop_term;
            factor = dop_term + sqrt(pow(dop_term,2.0)+1.);
            fai[i][j] = - log(dop_term);
        }
        fai[i][j] = fai[i][j] + V_substrate;
    }
    
    //     Gate contact
    for (i = n_s+1; i < n_d-1; i++) {
        beta[i] = gamma_c*(xx[i]+xx[i-1])/oxide_thickness * (delta_Ec+V_gate);
    }
    
    return 0;
}


int electric_field_update() {
    
    int i, j;
    
    inv_mesh_sizex = 1./mesh_sizex;
    inv_mesh_sizey = 1./mesh_sizey;
    
    for (i = 0; i < nx_max-1; i++) {
        for (j = 0; j < ny_max; j++) {
            fx_field[i][j] = (fai2[i][j] - fai2[i+1][j]) / mesh_sizex*Vt;
        }
    }
    
    for (i = 0; i < nx_max; i++) {
        for (j = 0; j < ny_max-1; j++) {
            fy_field[i][j] = (fai2[i][j] - fai2[i][j+1]) / mesh_sizey*Vt;
        }
    }
    
    return 0;
}

int init_realspace(int ne, int i, int j) {
    
    //     Define particle coordinates based on grid location
    
    if((i != 0) && (i != nx_max)) {
        x_position = (float(i)+((double)rand()/RAND_MAX)-0.5)*mesh_sizex;
        goto lbl_IR_11;
    } else if(i == 0) {
        x_position = 0.5*((double)rand()/RAND_MAX)*mesh_sizex;
        goto lbl_IR_11;
    } else if(i == nx_max) {
        x_position = device_le-0.5*((double)rand()/RAND_MAX)*mesh_sizex;
    }
    
lbl_IR_11:
    
    if(x_position < 0) { x_position = -x_position; }
    if(x_position > device_length) {
        factor = abs(x_position - device_length);
        x_position = device_length - factor;
    }
    
    if((j != 0) && (j != ny_max)) {
        y_position = (float(j)+((double)rand()/RAND_MAX)-0.5)*mesh_sizey;
        goto lbl_IR_22;
    } else if(j == 0) {
        y_position = 0.5*((double)rand()/RAND_MAX)*mesh_sizey;
        goto lbl_IR_22;
    } else if(j == ny_max) {
        y_position = device_depth-0.5*((double)rand()/RAND_MAX)*mesh_sizey;
    }
    
lbl_IR_22:
    
    if(y_position < 0) { y_position = -y_position; }
    if(y_position > device_depth) {
        factor = abs(y_position - device_depth);
        y_position = device_depth - factor;
    }
    
    //      Map particle atributes
    
    p[ne][5] = x_position;
    p[ne][6] = y_position;
	   
    return 0;
}

int init_kspace(int ne, int i, int j) {
    
    double rr, ct, st, tc, fai;
    
    //      Set particle energy
    
    e = -(1.5*Vt)*log(rand()/(double)RAND_MAX);
    
    //      Set initial valley index and region
    
    rr = 3.0*(double)rand()/RAND_MAX;
    if (rr <= 1.) {
        iv = 1;
    } else if (rr <= 2.) {
        iv = 2;
    } else if (rr <= 3.) {
        iv = 3;
    }
    
    iv = 1;
    i_region = find_region(i, j);
    
    //      Initial wavevector
    
    k = smh[iv]*sqrt(e*(1.+af[iv]*e));
    fai = two_pi*(double)rand()/RAND_MAX;
    ct = 1.-2.*(double)rand()/RAND_MAX;
    st = sqrt(1.-ct*ct);
    kx = k*st*cos(fai);
    ky = k*st*sin(fai);
    kz = k*ct;
    
    //      Check boundaries
    
    if ((i == 0) && (kx < 0)) { kx = -kx; }
    if ((i == nx_max) && (kx > 0)) { kx = -kx; }
    if ((j == 0) && (ky < 0)) { ky = -ky; }
    if ((j == ny_max) && (ky == 0)) { ky = -ky; }
    
    //      Initial free-flight
    
lbl_init_103:    rr = (double)rand()/RAND_MAX;
    
    if (rr <= 1.e-5) { goto lbl_init_103;}
    
    tc = -(log(rr))*tau_max[iv];
    
    //      Map particle atributes
    
    p[ne][1] = kx;
    p[ne][2] = ky;
    p[ne][3] = kz;
    p[ne][4] = tc;
    p[ne][8] = i_region;
    ip[ne] = iv;
    energy[ne] = e;
	   
    return 0;
}

int electrons_initialization() {
    
    double cell_volume;
    int i, j, m, n_electron;
    
    cell_volume = mesh_sizex*mesh_sizey*device_width;
    
    n_electron = 0;
    for (i = 0; i < nx_max; i++) {
        for (j = 0; j < ny_max; j++) {
            denn = exp(fai[i][j])*intrinsic_carrier_density;
            if((i == 0) || (i == nx_max)) { denn = denn/2.; }
            if((j == 0) || (j == ny_max)) { denn = denn/2.; }
            np_ij = int(denn*cell_volume);
            
            if (np_ij == 0) { goto lbl_EI_20; }
            
            for (m = 1; m < np_ij; m++) {
                ne = ne + 1;
                
                if(ne > max_electron_number) {
                    printf("Actual number of electrons = %d \n",ne);
                    printf("No of electrons exceeds %d \n \n",max_electron_number);
                    std::exit;
                }
                
                init_kspace(ne,i,j);
                init_realspace(ne,i,j);
                
            }
        lbl_EI_20:     continue;
        }
    }
    
    n_used = ne;
    printf("Maximum # of electrons allowed = %d \n", max_electron_number);
    printf("Number of electrons initialized = %d \n", n_used);
    
    for (i = n_used + 1; i < max_electron_number; i++) {
        ip[i] = 9;
    }
    
    return 0;
}

int check_boundary() {
    
    /*common
    &/mesh/mesh_sizex,mesh_sizey
    &/length/device_length
    &/depth/device_depth
    &/width/device_width
    &/mesh_size/nx_max,ny_max
    &/source_drain/n_s,n_d
    &/particle_atr/kx,ky,kz,iv,e,i_region,
    1              x_position,y_position,ix_fix,jy_fix
    &/drain_current/idd_out,idd_eli,idd_cre
    &/source_current/iss_out,iss_eli,iss_cre
    
    real kx,ky,kz,e,mesh_sizex,mesh_sizey
    integer iv,i_region,ix_fix,jy_fix
    integer ix,jy,n_s,n_d*/
    
    //     Check side and bottom boundary
    
    if (x_position < 0.) {
        x_position = -x_position;
        kx = -kx;
    } else if(x_position > device_length) {
        x_position = device_length - (x_position - device_length);
        kx = -kx;
    } else if(y_position > device_depth) {
        std::cout << "reached bottom boundary \n";
        y_position = device_depth - (y_position - device_depth);
        ky = -ky;
        //        iv = 9;
    }
    
    //     Check top boundary
    
    if (y_position <= 0) {
        ix = int(x_position / mesh_sizex + 0.5);
        if (ix > nx_max) { ix = nx_max; }
        if (ix < 0) { ix = 0; }
        if (ix <= n_s) {
            iv = 9;
            iss_out = iss_out + 1;
            goto lbl_CB_11;
        } else if (ix >= n_d) {
                iv = 9;
                idd_out = idd_out + 1;
                goto lbl_CB_11;
        } else if ((ix > n_s) && (ix < n_d)) {
                    y_position = -y_position;
                    ky = -ky;
        }
    }

lbl_CB_11:
    return 0;

}

int count_used_particles(int ne) { // ???????
    
    int i;
    ne = 0;
    
    for (i = 1; i < max_electron_number; i++) {
        if(ip[i] != 9) { ne = ne + 1; }
    }
    
    return 0;
}

int check_source_drain_contacts() {
    
    //    Delete extra carriers at the source and drain contacts
    
    //     print*,'Enter the delete portion at the contacts'
    //      print*,'Number of particles used = ',n_used
    
    int i, n, nn, nele_diff;
    
    for (i = 0; i < nx_max; i++) {
        npt[i] = 0;
    }
    
    for (n = 1; n < n_used; n++) {
        if (ip[n] != 9) {
            x_position = p[n][5];
            y_position = p[n][6];
            //        z_position = p[n][7]
            ix = int(x_position / mesh_sizex + 0.5);
            jy = int(y_position / mesh_sizey + 0.5);
            if (ix > nx_max) { ix = nx_max; }
            n_number = nsource_drain_carriers[ix];
            if ((jy == 0) && ((ix <= n_s) || (ix >= n_d))) {
                if (npt[ix] < n_number) {
                    npt[ix] = npt[ix] + 1;
                } else {
                    ip[n] = 9;
                    if (ix >= n_d) { idd_eli = idd_eli + 1; }
                    if (ix <= n_s) { iss_eli = iss_eli + 1; }
                }
            }
        }
    }
    
    
    jy = 0;
    for (ix = 0; ix < nx_max; ix++) {
        if((ix <= n_s) || (ix >= n_d)) {
            nele_diff = nsource_drain_carriers[ix] - npt[ix];
            if (nele_diff <= 0) goto lbl_CSDC_11;
            if (nele_diff > 0) {
                for (nn = 1; nn < nele_diff; nn++) {
                    
                    ne = n_used + nn;
                    init_kspace(ne, ix, jy);
                    init_realspace(ne, ix, jy);
                    
                }
                
                n_used = n_used + nele_diff;
                if(ix >= n_d){ idd_cre = idd_cre + nele_diff; }
                if(ix <= n_s){ iss_cre = iss_cre + nele_diff; }
                
            }
        lbl_CSDC_11:       continue;
        }
    }
    
    //      print*,'idd_cre = ',idd_cre;
    //      print*,'iss_cre = ',iss_cre;
    //      call count_used_particles(ne);
    //      print*,'Number of electrons in use = ',ne;
    //      print*,'Number of particles used = ',n_used;
    //      print*,'End of the create portion of the code';
    //      print*,'  ';
    
    return 0;
    
}

int delete_particles() {
    
    int i, j, n_fix;
    
    for (i = 1; i < n_used; i++) {
        
        if (ip[i] == 9) {
            flag_conv = false;
            n_fix = n_used + 1;
            
            while(!flag_conv) {
                if(ip[n_fix] != 9) {
                    ip[i] = ip[n_fix];
                    
                    j = 1;
                    while (j != 8) {
                        j = j+1;
                        p[i][j] = p[n_fix][j];
                    }
                    
                    energy[i] = energy[n_fix];
                    ip[n_fix] = 9;
                    n_fix = n_fix - 1;
                    flag_conv = true;
                }
                n_fix = n_fix - 1;
                if(n_fix < i) { flag_conv = true; }
            }
        }
        
    }
    
lbl_DP_14:
    
    if(ip[n_used] == 9) {
        n_used = n_used - 1;
        goto lbl_DP_14;
    }
    
    //      call count_used_particles(ne)
    //      print*,'Number of particles used = ',n_used
    //      print*,'Number of electrons in use = ',ne
    //      print*,'End of the delete subroutine'
    //      print*,'  '
    
    return 0;
}

int local_field_value_NEC() {
    /*include 'max_grid_size.h'
    
    common
    &/mesh_size/nx_max,ny_max
    &/force/fx_field(0:nx_size,0:ny_size),
    1       fy_field(0:nx_size,0:ny_size)
    &/particle_atr/kx,ky,kz,iv,e,i_region,
    1              x_position,y_position,ix_fix,jy_fix
    
    real kx,ky,kz,e,x_position,y_position
    real fx,fy,fz
    integer iv,i_region,ix_fix,jy_fix,ix,jy*/
    
    ix = ix_fix;
    jy = jy_fix;
    if (ix >= nx_max) { ix = nx_max-1; }
    if(jy >= ny_max) { jy = ny_max-1; }
    if (ix < 0) { ix = 0; }
    if (jy < 0) { jy = 0; }
    fx = 0.5*(fx_field[ix][jy] + fx_field[ix][jy+1]);
    fy = 0.5*(fy_field[ix][jy] + fy_field[ix+1][jy]);
    
    return 0;
}

int charge_assignment_NEC() {
    
    int i, j, n;
    double cell_volume, norm_factor;
    
    //     Evaluate multiplication constant
    
    cell_volume = mesh_sizex * mesh_sizey * device_width;
    norm_factor = cell_volume * intrinsic_carrier_density;
    norm_factor = 1./norm_factor;
    
    //     Reset the charge vector
    
    for (i = 0; i < nx_max; i++) {
        for (j = 0; j < ny_max; j++) {
            elec_number[i][j] = 0.;
        }
    }
    
    //     Charge assignment part
    
    for (n = 1; n < n_used; n++) {
        if(ip[n] != 9) {
            i = int(p[n][5]/mesh_sizex);
            j = int(p[n][6]/mesh_sizey);
            if(i < 0) { i = 0; }
            if(i >= nx_max) { i=nx_max-1; }
            if(j < 0) { j=0; }
            if(j >= ny_max) { j = ny_max-1; }
            elec_number[i][j] = elec_number[i][j]+0.25;
            elec_number[i][j+1] = elec_number[i][j+1]+0.25;
            elec_number[i+1][j] = elec_number[i+1][j]+0.25;
            elec_number[i+1][j+1] = elec_number[i+1][j+1]+0.25;
        }
    }
    
    //     Calculate electron density
    
    for (i = 0; i < nx_max; i++) {
        for (j = 0; j < ny_max; j++) {
            denn = elec_number[i][j];
            
            if((i == 0) || (i == nx_max)) { denn = 2.*denn; }
            if((j == 0) || (j == ny_max)) { denn = 2.*denn; }
            
            electron_density[i][j] = denn*norm_factor;
        }
    }
    
    return 0;
}
