//
//  device.hpp
//  DeviceSim
//
//  Created by Daniel Livingston on 10/5/15.
//  Copyright © 2015 Daniel Livingston. All rights reserved.
//

#ifndef device_hpp
#define device_hpp

#include <stdio.h>

int device_structure_initialization();
int source_drain_carrier_number();
int apply_voltage();
int electric_field_update();
int init_realspace(int ne, int i, int j); // <-- be careful about the int ne
int init_kspace(int ne, int i, int j);
int electrons_initialization();
int check_boundary();
int count_used_particles(int ne);
int check_source_drain_contacts();
int delete_particles();
int local_field_value_NEC();
int charge_assignment_NEC();

#endif /* device_hpp */
