//
//  poisson_SOR.hpp
//  DeviceSim
//
//  Created by Daniel Livingston on 10/5/15.
//  Copyright © 2015 Daniel Livingston. All rights reserved.
//

#ifndef poisson_SOR_hpp
#define poisson_SOR_hpp

#include <stdio.h>
#include <cmath>

int domain_boundaries(int n_reg, int i_reg_min, int i_reg_max, int j_reg_min, int j_reg_max);
int find_region(int i, int j);
int initialize_doping_potential();
int coefficients_poisson();
int mesh_initialization();
int poisson_SOR(double flag_solution);

#endif /* poisson_SOR_hpp */
