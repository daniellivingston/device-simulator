//
//  poisson_SOR.cpp
//  DeviceSim
//
//  Created by Daniel Livingston on 10/5/15.
//  Copyright © 2015 Daniel Livingston. All rights reserved.
//

#include "poisson_SOR.hpp"
#include "readin.hpp"

int domain_boundaries(int n_reg, int i_reg_min, int i_reg_max, int j_reg_min, int j_reg_max) {
    
    ix_min[n_reg] = i_reg_min;
    ix_max[n_reg] = i_reg_max;
    jy_min[n_reg] = j_reg_min;
    jy_max[n_reg] = j_reg_max;
    
    return 0;
}

int find_region(int i, int j) {
    
    int n, i_reg;
    
    for (n = 1; n < n_region; n++) {
        if((i >= ix_min[n]) && (i <= ix_max[n]) && (j >= jy_min[n]) && (j <= jy_max[n])) {
            i_reg = n;
            goto lbl_11_fr;
        }
    }
    
lbl_11_fr:
    //find_region = i_reg;
    
    return i_reg;
}


int initialize_doping_potential() {
    
    int i, j, n;
    
    for (i = 0; i < nx_max; i++) {
        for (j = 0; j < ny_max; j++) {
            n = find_region(i, j);
            
            dop_term = doping_density[n]/intrinsic_carrier_density;
            doping[i][j] = dop_term;
            dop_term = 0.5*dop_term;
            if(dop_term > 0) {
                factor = dop_term + sqrt(pow(dop_term,2.0) + 1.);
                fai[i][j] = log(dop_term);
            } else if(dop_term <= 0) {
                dop_term = -dop_term;
                factor = dop_term + sqrt(pow(dop_term,2.0) + 1.);
                fai[i][j] = -log(dop_term);
            }
            // 11        continue;
        }
    }
    
    return 0;
}

int coefficients_poisson() {
    
    int i, j;
    
    for (i = 0; i < nx_max; i++) {
        for (j = 0; j < ny_max; j++) {
            if (flag_dom[i][j] == 3) {
                a_coeff[i][j] = (yy[j] + yy[j-1]) / xx[i];
                b_coeff[i][j] = (yy[j] + yy[j-1]) / xx[i-1];
                
                if (i == nx_max) {
                    a_coeff[i][j] = 0;
                    b_coeff[i][j] = 2.*b_coeff[i][j];
                } else if(i == 0) {
                    a_coeff[i][j] = 2.*a_coeff[i][j];
                    b_coeff[i][j] = 0.;
                }
                
                
                d_coeff[i][j] = (xx[i] + xx[i-1]) / yy[j];
                e_coeff[i][j] = (xx[i] + xx[i-1]) / yy[j-1];
                
                if (j == ny_max) {
                    d_coeff[i][j] = 0.;
                    e_coeff[i][j] = 2. * e_coeff[i][j];
                } else if (j == 0) {
                    e_coeff[i][j] = 0.;
                    d_coeff[i][j] = 2. * d_coeff[i][j];
                }
                
                c_coeff[i][j] = - (a_coeff[i][j] + b_coeff[i][j] + d_coeff[i][j] + e_coeff[i][j]);
                alpha[i][j] = 0.5*(xx[i] + xx[i-1]) * (yy[j] + yy[j-1]);
                
            } else if (flag_dom[i][j] == 1.) {
                a_coeff[i][j] = 0.;
                b_coeff[i][j] = 0.;
                d_coeff[i][j] = 0.;
                e_coeff[i][j] = 0.;
                c_coeff[i][j] = 1.;
            } else if(flag_dom[i][j] == 2.) {
                a_coeff[i][j] = yy[j] / xx[i];
                b_coeff[i][j] = yy[j] / xx[i-1];
                d_coeff[i][j] = (xx[i] + xx[i-1]) / yy[j];
                e_coeff[i][j] = 0.;
                c_coeff[i][j] = -(a_coeff[i][j] + b_coeff[i][j] + d_coeff[i][j] * (1.+gamma_c*yy[j]/oxide_thickness));
                alpha[i][j] = 0.5*yy[j]*(xx[i] + xx[i-1]);
            }
        }
    }
    
    return 0;
}

int mesh_initialization() {
    
    int i, j;
    
    for (i = -1; i < nx_max; i++) {
        xx[i] = mesh_sizex/debye_length;
    }
    
    for (j = -1; j < ny_max; j++) {
        yy[j] = mesh_sizey/debye_length;
    }
    
    return 0;
}


int poisson_SOR(double flag_solution) {
    
    double denn_sum, temp_term;
    
    int i, j;
    
    
    for (i = 0; i < nx_max; i++) {
        for (j = 0; j < ny_max; j++) {
            
            if (flag_dom[i][j] == 1) {
                forcing_func[i][j] = fai[i][j];
                c2[i][j] = c_coeff[i][j];
            }
            
        }
    }
    
    while (!flag_conv) {
        
        //     Calculate the forcing function
        
        for (i = 0; i < nx_max; i++) {
            for (j = 0; j < ny_max; j++) {
                if(flag_dom[i][j] == 1) { goto lbl_11_SOR; }
                
                if(flag_solution == 0.) {
                    
                    denn = exp(fai[i][j]);
                    denn_inv = 1./denn;
                    
                } else if(flag_solution == 1) {
                    
                    denn = electron_density[i][j];
                    denn_inv = exp(-fai[i][j]);
                    
                }
                
                denn_sum = denn + denn_inv;
                
                if(flag_dom[i][j] == 3) {
                    
                    c2[i][j] = c_coeff[i][j] - alpha[i][j] * denn_sum;
                    temp_term = denn_inv - denn + doping[i][j] + fai[i][j]*denn_sum;
                    forcing_func[i][j] = -alpha[i][j]*temp_term;
                    goto lbl_11_SOR;
                    
                } else if(flag_dom[i][j] == 2) {
                    
                    c2[i][j] = c_coeff[i][j] - alpha[i][j] * denn_sum;
                    temp_term = denn_inv - denn + doping[i][j] + fai[i][j] * denn_sum;
                    forcing_func[i][j] = -alpha[i][j] * temp_term - beta[i];
                    
                }
                
            lbl_11_SOR:
                continue;
                
            }
        }
        
        //     Update potential
        
        double x1, x2, newerror;
        double error = 0.;
        
        for (i = 0; i < nx_max; i++) {
            for (j = 0; j < ny_max; j++) {
                x1 = (forcing_func[i][j] - a_coeff[i][j] * fai[i+1][j] - b_coeff[i][j] * fai[i-1][j] - d_coeff[i][j] * fai[i][j+1] - e_coeff[i][j] * fai[i][j-1]) / c2[i][j];
                x2 = fai[i][j] + omega_c*(x1 - fai[i][j]);
                newerror = abs(x2-fai[i][j]);
                if (newerror > error) { error = newerror; }
                fai[i][j] = x2;
            }
        }
        
        if (error < tolerance) { flag_conv = true; }
        
    }
    
    return 0;
}
