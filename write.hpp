//
//  write.hpp
//  DeviceSim
//
//  Created by Daniel Livingston on 10/5/15.
//  Copyright © 2015 Daniel Livingston. All rights reserved.
//

#ifndef write_hpp
#define write_hpp

#include <stdio.h>

int write_mesh();
int write_electron_distribution();
int write_electric_field();
int write_potential(int flag_solution);
int velocity_energy_cumulative(int iter_reference, int n_time_steps_av);

#endif /* write_hpp */
