//
//  junk.cpp
//  DeviceSim
//
//  Created by Daniel Livingston on 10/7/15.
//  Copyright © 2015 Daniel Livingston. All rights reserved.
//

#include "junk.hpp"
#include "readin.hpp"

void acoustic_rate(int i_count, int i_region, std::string out_file) {
    
    double acoustic;
    double ee;
    
    double c_l = density*sound_velocity*sound_velocity;
    double p_const = sqrt(2.)*kb*tem/pi/h/c_l*(amd/h)*(sqrt(amd)/h*sqrt(q))*(qh*q)*sigma*sigma;
    
    std::string path_ac = working_dir + "ACOUSTIC";
    std::ofstream file_ac(path_ac.c_str());
    
    i_count = i_count + 1;
    
    for (int i = 1; i < n_lev; i++) {
        ee = de*float(i);
        fe = ee*(1.+af*ee);
        acoustic = p_const*sqrt(fe)*(1.+af2*ee);
        scatt_table(i,i_count,i_region) = acoustic;
        file_ac << ee << " " << acoustic << std::endl;
    }
    
    file_ac.close();
    
    flag_mech[i_count][i_region] = 1;
    w[i_count][i_region] = 0.;
    
}

void Coulomb_BH(int i_count, int i_region, std::string out_file_1) {
    
    Debye_length = sqrt(eps_sc*Vt/q/doping_density[i_region]);
    Energy_debye[i_region] = hhm/Debye_length/Debye_length;
    final_mass = amd;
    
    factor = Debye_length*Debye_length/eps_sc;
    double p_const = doping_density[i_region]*final_mass*qh*sqrt(2.*final_mass)/pi*qh*factor*qh*factor*qh*sqrt(q);
    
    i_count = i_count + 1;
    
    std::string path_coulomb = working_dir + "COULOMB";
    std::ofstream file_co(path_coulomb.c_str());
    
    for (int i = 1; i < n_lev; i++) {
        ee = de*float(i);
        ge = ee*(1.af*ee);
        
        factor = sqrt(ge)*(1.+af2*ee)/(1.+4.*ge/Energy_debye[i_region]);
        scatt_rate = p_const*factor;
        scatt_table[i][i_count][i_region] = scatt_rate;
        
        file_co << ee << " " << scatt_rate << std::endl;
        
    }
    
    file_co.close();
    
    flag_mech[i_count][i_region] = 3;
    w[i_count][i_region] = 0.;
    
}