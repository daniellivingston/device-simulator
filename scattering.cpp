//
//  scattering.cpp
//  DeviceSim
//
//  Created by Daniel Livingston on 10/5/15.
//  Copyright © 2015 Daniel Livingston. All rights reserved.
//

// =========
//  Note: For the local variables declared in drift, they may actually be global
//        If code still acting weird, double check
//        Also, tm[i] -> tm2[i] ?

#include "scattering.hpp"
#include "poisson_SOR.hpp"
#include "device.hpp"
#include "readin.hpp"
#include <fstream>

// Drift //////////////////////////////

int drift(double tau) {
    
    double qh1, dkx, dky, dkz, kxf, kyf, kzf;
    double skx, sky, skz, sk, gk, dx, dy, dz;
    
     //      Calculate electric field
     
    local_field_value_NEC();
     
    //       Update particle momentum and energy
     
    qh1 = -qh*tau;
    if(iv == 1) {
        dkx = qh1*fx*tm2[1];
        dky = qh1*fy*tm2[2];
     //          dkz=qh1*fz*tm(3)
    } else if(iv == 2) {
        dkx = qh1*fx*tm2[2];
        dky = qh1*fy*tm2[1];
     //          dkz=qh1*fz*tm(3)
    } else if(iv == 3) {
        dkx = qh1*fx*tm2[3];
        dky = qh1*fy*tm2[2];
     //          dkz=qh1*fz*tm(1)
    }
    dkz = 0.;
     
    kxf = kx+dkx;
    kyf = ky+dky;
    kzf = kz+dkz;
     
    skx = kxf*kxf;
    sky = kyf*kyf;
    skz = kzf*kzf;
    sk = skx+sky+skz;
    gk = hhm[iv]*sk;
    //    gk = hhm*sk; <-- Original
    e = 2*gk/(1.+sqrt(1.+af4[iv]*gk)); // <-- af4*gk is original
     
     //      Update particle's position using the 'leap-frog' scheme
     
    double avkx = 0.5*(kx+kxf);
    double avky = 0.5*(ky+kyf);
    double avkz = 0.5*(kz+kzf);
     
    kx = kxf;
    ky = kyf;
    kz = kzf;
     
    double denom = tau/(1.+af2[iv]*e);
    if(iv == 1) {
        dx = hm[1]*avkx*denom;
        dy = hm[2]*avky*denom;
     //         dz=hm(3)*avkz*denom
    } else if(iv == 2) {
        dx = hm[2]*avkx*denom;
        dy = hm[1]*avky*denom;
     //         dz=hm(3)*avkz*denom
    } else if(iv == 3) {
        dx = hm[3]*avkx*denom;
        dy = hm[2]*avky*denom;
     //         dz=hm(1)*avkz*denom
    }
    
    x_position = x_position + dx;
    y_position = y_position + dy;
     //      z_position = z_position + dz
     
     //     Check simulation boundaries
     
    check_boundary();
    
    if(iv != 9) {
        ix_fix = int(x_position/mesh_sizex);
        
        if(ix_fix > nx_max) { ix_fix = nx_max; }
        
        jy_fix = int(y_position/mesh_sizey);
        
        if(jy_fix > ny_max) { jy_fix = ny_max; }
        
        i_region = find_region(ix_fix,jy_fix);
    }
    
    return 0;
    /*
    qh1 = qh*tau;
    dkx = -qh1*fx;
    dky = -qh1*fy;
    dkz = -qh1*fz;
    
    kx = kx+dkx;
    ky = ky+dky;
    kz = kz+dkz;
    
    skx = kx*kx;
    sky = ky*ky;
    skz = kz*kz;
    sk = skx+sky+skz;
    k = sqrt(sk);
    gk = hhm[iv]*sk;
    e = 2*gk/(1.+sqrt(1.+af4[iv]*gk));
    
    return 0;*/
}

// Isotropic //////////////////////////////

int isotropic(int i_fix) {
    
    double ct, rknew, fi, st;
    
    //  Update carrier energy
    e = e + w[i_fix][iv];
    
    //  Update carrier wavevector
    rknew = smh[i_final]*sqrt(e*(1.+af[i_final]*e));
    
    fi = two_pi*rand()/(double)RAND_MAX;
    ct = 1.-2.*rand()/(double)RAND_MAX;
    st = sqrt(1.-ct*ct);
    
    kx = rknew*st*cos(fi);
    ky = rknew*st*sin(fi);
    kz = rknew*ct;
    
    return 0;
}

////// ISOTROPIC_G* //////////////////////////////

int isotropic_g(int i_fix) {
    
    double rknew, ct, st, fi;
    
    e = e + w[i_fix][iv];
    //e = e + w[i_fix][i_region];
    
    //rknew = smh[iv]*sqrt(e*(1.+af[iv]*e));
    rknew = smh[i_final]*sqrt(e*(1.+af[i_final]*e));
    
    fi = two_pi*rand()/(double)RAND_MAX;
    ct = 1.-2.*rand()/(double)RAND_MAX;
    st = sqrt(1.-ct*ct);
    
    kx = rknew*st*cos(fi);
    ky = rknew*st*sin(fi);
    kz = rknew*ct;
    
    return 0;
    
}

////// ISOTROPIC_F* /////////////////////////////////

int isotropic_f(int i_fix) {
    
    double rknew, ct, st, fi;
    int iv0 = 0;
    
    e = e + w[i_fix][iv];
    //    e = e + w[i_fix][i_region];
    
    //rknew = smh[iv]*sqrt(e*(1+af[iv]*e));
    rknew = smh[i_final]*sqrt(e*(1+af[i_final]*e));
    fi = two_pi*rand()/(double)RAND_MAX;
    ct = 1.-2.*rand()/(double)RAND_MAX;
    st = sqrt(1.-ct*ct);
    
    kx = rknew*st*cos(fi);
    ky = rknew*st*sin(fi);
    kz = rknew*ct;
    
    double rr = rand()/(double)RAND_MAX;
    
    if (iv == 1) {
        
        if (rr <= 0.5) {
            iv0 = 2;
        } else {
            iv0 = 3;
        }
        
    }
    
    if (iv == 2) {
        if (rr <= 0.5) {
            iv0 = 1;
        } else {
            iv0 = 3;
        }
    }
    
    if (iv == 3) {
        if (rr <= 0.5) {
            iv0 = 1;
        } else {
            iv0 = 2;
        }
    }
    
    iv = iv0;
    
    return 0;
}

// Polar Optical Angle //////////////////////////////

int polar_optical_angle(int i_fix) {
    
    double enew, gnew, zeta, ge, rr;
    double cth0, sth0, cfi0, sfi0, cth, sth, fi, cfi, sfi;
    
    //  Update carrier energy
    enew = e + w[i_fix][iv];
    
    //  Calculate the rotation angles
    kxy = sqrt(kx*kx+ky*ky);
    k = sqrt(kxy*kxy+kz*kz);
    cth0 = kz/k;
    sth0 = kxy/k;
    cfi0 = kx/kxy;
    sfi0 = ky/kxy;
    
    //  Randomize momentum in the rotated coordinate system
    kp = smh[iv]*sqrt(enew*(1.0+af[iv]*enew));
    ge = e*(1.0+af[iv]*e);
    gnew = enew*(1.0+af[iv]*enew);
    zeta = 2.0*sqrt(ge*gnew)/(ge+gnew-2.0*sqrt(ge*gnew));
    
    rr = rand()/(double)RAND_MAX;
    
    // cth is the problem line, that eventually runs
    //     energy throught the ground, vicariously
    //     thorugh kx, ky, kz
    
    cth = ((zeta+1)-pow((2*zeta + 1),rr))/zeta;
    //  cth = 1 - rr; // Temporary approximation
    sth = sqrt(1.-cth*cth);
    
    fi = two_pi*rand()/(double)RAND_MAX;
    cfi = cos(fi);
    sfi = sin(fi);
    kxp = kp*sth*cfi;
    kyp = kp*sth*sfi;
    kzp = kp*cth;
    
    //  Return back to the original coordinate system
    kx = kxp*cfi0*cth0-kyp*sfi0+kzp*cfi0*sth0;
    ky = kxp*sfi0*cth0+kyp*cfi0+kzp*sfi0*sth0;
    kz = -kxp*sth0+kzp*cth0;
    
    e = enew;
    
    return 0;
}

// Coulomb Angle BH //////////////////////////////

int Coulomb_angle_BH() {
    
    double cth0, sth0, cfi0, sfi0, sfi;
    double ge, rr, cth, sth, fi, cfi;
    
    //     Update carrier energy
    //     enew = e + w(i_fix,iv)
    
    //     Calculate the rotation angles
    kxy = sqrt(kx*kx+ky*ky);
    k = sqrt(kxy*kxy+kz*kz);
    cth0 = kz/k;
    sth0 = kxy/k;
    cfi0 = kx/kxy;
    sfi0 = ky/kxy;
    
    //     Randomize momentum in the rotated coordinate system
    ge = e*(1.+af[iv]*e);
    
    rr = (double)rand()/RAND_MAX;
    
    cth = 1. - 2*rr/(1.+4.*(1-rr)*ge/Energy_debye[i_region]);
    sth = sqrt(1.-cth*cth);
    
    fi = two_pi*(double)rand()/RAND_MAX;
    
    cfi = cos(fi);
    sfi = sin(fi);
    kxp = k*sth*cfi;
    kyp = k*sth*sfi;
    kzp = k*cth;
    
    //     Return back to the original coordinate system
    kx = kxp*cfi0*cth0-kyp*sfi0+kzp*cfi0*sth0;
    ky = kxp*sfi0*cth0+kyp*cfi0+kzp*sfi0*sth0;
    kz = -kxp*sth0+kzp*cth0;
    
    //     e = enew
    
    return 0;
}

// Scatter Carrier //////////////////////////////

int scatter_carrier() {
    
    double rr, bound_lower, bound_upper;
    int i, i_top, i_fix, select_mech;
    unsigned int loc;
    
    //     Calculate index to the scattering table
    
    loc = (int)(e/de);
    if (loc == 0) { loc = 1; }
    if (loc > n_lev) { loc = n_lev; }
    
    
    // Select scattering mechanism
    
    i_top = max_scatt_mech[iv];
    rr = rand()/(double)RAND_MAX;
    
    if (rr >= scatt_table[loc][i_top][iv]) { // i_region?
        freq[i_top+1][iv] = freq[i_top+1][iv] + 1;
        goto lbl_222;
    }
    
    if (rr < scatt_table[loc][1][iv]) {
        i_fix = 1;
        freq[i_fix][iv] = freq[i_fix][iv] + 1;
        goto lbl_111;
    }
    
    if (i_top > 1) {
        for (i = 1; i <= i_top-1; i++) {
            bound_lower = scatt_table[loc][i][iv];
            bound_upper = scatt_table[loc][i+1][iv];
            
            if ((rr >= bound_lower) && (rr < bound_upper)) {
                i_fix = i+1;
                freq[i_fix][iv] = freq[i_fix][iv] +1;
                goto lbl_111;
            }
            
        }
    }
    
lbl_111:
    
    //     Perform scattering (change energy and randomize momentum)
    
    select_mech = flag_mech[i_fix][iv];
    i_final = i_valley[i_fix][iv];
    
    if (select_mech == 1) {
        isotropic_g(i_fix); //isotropic(i_fix);
    } else if (select_mech == 2 ) {
        isotropic_f(i_fix); //polar_optical_angle(i_fix);
    } else if (select_mech == 3) {
        Coulomb_angle_BH();
    }
    
    iv = i_final;
    
lbl_222:
    
    return 0;
}

// Free Flight Scatter //////////////////////////////
// Altered to match the device sim
int free_flight_scatter() {
    
    double rr;
    double dtau, dte, dte2, dt2, dt3, dtp;
    
    inv_mesh_sizex = 1./mesh_sizex; // new
    inv_mesh_sizey = 1./mesh_sizey; // new
    
    idd_out = 0;
    idd_eli = 0;
    idd_cre = 0;
    iss_out = 0;
    iss_eli = 0;
    iss_cre = 0;
    
    // Reset counter for scattering frequency
    
    int i;
    int j;
    
    for (i = 1; i <= 10; i++) {
        for (j = 1; j <= 3; j++) {
            freq[i][j] = 0.;
        }
    }
    
    for (i = 1; i <= n_used; i++) { // n_used from nsim
        
        
        // Inverse mapping of particle atributes
        
        kx = p[i][1];
        ky = p[i][2];
        kz = p[i][3];
        dtau = p[i][4];
        //        i_region = (int)p[i][8];
        x_position = p[i][5];
        y_position = p[i][6];
        // z_position = p(i,7)
        i_region = int(p[i][8]);
        iv = ip[i];
        e = energy[i];
        
        ix_fix = int(x_position/mesh_sizex);
        jy_fix = int(y_position/mesh_sizey);
        
        if(ix_fix < 0) { ix_fix = 0; }
        if(ix_fix > nx_max) { ix_fix = nx_max; }
        if(jy_fix < 0) { jy_fix = 0; }
        if(jy_fix > ny_max) { jy_fix = ny_max; }
        if(iv == 9) { goto lbl_403; }
        
        // Initial free-flight of the carriers
        
        dte = dtau;
        
        if (dte >= dt) {
            dt2 = dt;
        } else {
            dt2 = dte;
        }
        
        drift(dt2);
        
        if (iv == 9) { goto lbl_403; } // this is where i stopped translating
        if (dte > dt) { goto lbl_401; }
        
        // Free-flight and scatter part
        
    lbl_402:
        dte2 = dte;
        
        scatter_carrier();
        
    lbl_219:
        rr = (double)rand()/RAND_MAX;
        
        
        if (rr <= 1e-6) { goto lbl_219; }
        
        
        dt3 = -(log(rr))*tau_max[iv]; // [i_region] ?
        dtp = dt - dte2;	// remaining time to scatter in dt-interval
        
        if (dt3 <= dtp) {
            dt2 = dt3;
        } else {
            dt2 = dtp;
        }
        
        drift(dt2);
        
        if (iv == 9) { goto lbl_403; } // added line
        
        // Update times
        
        dte2 = dte2 + dt3;
        dte = dte2;
        
        if (dte < dt) { goto lbl_402; }
        
    lbl_401:
        dte = dte - dt;
        dtau = dte;
        
    lbl_403:
        
        // Map particle atributes
        
        p[i][1] = kx;
        p[i][2] = ky;
        p[i][3] = kz;
        p[i][4] = dtau;
        p[i][5] = x_position;
        p[i][6] = y_position;
        //p[i][7] = z_position;
        p[i][8] = i_region;
        ip[i] = iv;
        energy[i] = e;
        //        p[i][8] = i_region;
    }
    
    return 0;
}

// Free Flight Scatter NEW //////////////////////////////

int free_flight_scatter_new() {
    
    double rr;
    double dtau, dte, dte2, dt2, dt3, dtp;
    
    // Reset counter for scattering frequency
    
    int i;
    //int j;
    
    /* for (i = 1; i <= 10; i++) {
     for (j = 1; j <= 3; j++) {
     freq[i][j] = 0.;
     }
     }*/
    
    for (i = 1; i <= nsim; i++) {
        
        
        // Inverse mapping of particle atributes
        
        kx = p[i][1];
        ky = p[i][2];
        kz = p[i][3];
        dtau = p[i][4];
        iv = ip[i];
        e = energy[i];
        
        // Initial free-flight of the carriers
        
        dte = dtau;
        
        if (dte >= dt) {
            dt2 = dt;
        } else {
            dt2 = dte;
        }
        
        drift(dt2);
        
        if (dte > dt) {
            goto lbl_401;
        }
        
        // Free-flight and scatter part
        
    lbl_402:
        dte2 = dte;
        
        scatter_carrier();
        
    lbl_219:
        rr = rand()/(double)RAND_MAX;
        
        
        if (rr <= 1e-6) {
            goto lbl_219;
        }
        
        
        dt3 = -(log(rr))*tau_max[iv];
        dtp = dt - dte2;	// remaining time to scatter in dt-interval
        
        if (dt3 <= dtp) {
            dt2 = dt3;
        } else {
            dt2 = dtp;
        }
        
        drift(dt2);
        
        // Update times
        
        dte2 = dte2 + dt3;
        dte = dte2;
        
        if (dte < dt) {
            goto lbl_402;
        }
        
    lbl_401:
        dte = dte - dt;
        dtau = dte;
        
        // Map particle atributes
        
        p[i][1] = kx;
        p[i][2] = ky;
        p[i][3] = kz;
        p[i][4] = dtau;
        ip[i] = iv;
        energy[i] = e;
    }
    
    return 0;
}

//  Save Histograms //////////////////////////////

int histograms(std::string file_name) {
    
    std::ofstream outfile(file_name.c_str());
    
    outfile << "kx    ky    kz    energy" << std::endl;
    
    int i;
    for (i = 1; i <= nsim; i++) {
        kx = p[i][1];
        ky = p[i][2];
        kz = p[i][3];
        e = energy[i];
        outfile << kx << " " << ky << " " << kz << " " << e << std::endl;
    }
    
    outfile.close();
    return 0;
}

// Write //////////////////////////////

int write_m(int nsim, int iter, double time, bool flag_write) {
    int n_val = 3;
    
    double denom, velx, vely, velz;
    double nvaly[n_val], sume[n_val];
    double velx_sum[n_val], vely_sum[n_val], velz_sum[n_val];
    double velocity_x[n_val], velocity_y[n_val], velocity_z[n_val];
    
    int i;
    for (i = 1; i <= n_val; i++) {
        velx_sum[i] = 0.;
        vely_sum[i] = 0.;
        velz_sum[i] = 0.;
        sume[i] = 0.;
        nvaly[i] = 0;
    }
    
    for (i = 1; i <= nsim; i++) {
        iv = ip[i];
        ee = energy[i];
        denom = 1./(1.+af2[iv]*ee);
        velx = h*p[i][1]*denom/am[iv];
        vely = h*p[i][2]*denom/am[iv];
        velz = h*p[i][3]*denom/am[iv];
        velx_sum[iv] = velx_sum[iv] + velx;
        vely_sum[iv] = vely_sum[iv] + vely;
        velz_sum[iv] = velz_sum[iv] + velz;
        sume[iv] = sume[iv] + energy[i];
        nvaly[iv] = nvaly[iv] + 1;
        
    }
    
    for (i = 1; i <= n_val; i++) {
        if (nvaly[i] != 0) {
            velocity_x[i] = velx_sum[i]/nvaly[i];
            velocity_y[i] = vely_sum[i]/nvaly[i];
            velocity_z[i] = velz_sum[i]/nvaly[i];
            sume[i] = sume[i]/nvaly[i];
        }
    }
    
    if (flag_write == 0) {
        
        output1 << "time   vx_gamma    vx_L    vx_X" << std::endl;
        output2 << "time   vy_gamma    vy_L    vy_X" << std::endl;
        output3 << "time   vz_gamma    vz_L    vz_X" << std::endl;
        output4 << "time   Ek_gamma    Ek_L    Ek_X" << std::endl;
        output5 << "time   gamma    L_valley    X_valley" << std::endl;
        
    }
    
    if (iter % 4 == 0) {
        
        output1 << time << " " << velocity_x[1] << " " << velocity_x[2] << " " << velocity_x[3] << std::endl;
        output2 << time << " " << velocity_y[1] << " " << velocity_y[2] << " " << velocity_y[3] << std::endl;
        output3 << time << " " << velocity_z[1] << " " << velocity_z[2] << " " << velocity_z[3] << std::endl;
        output4 << time << " " << sume[1] << " " << sume[2] << " " << sume[3] << std::endl;
        output5 << time << " " << nvaly[1] << " " << nvaly[2] << " " << nvaly[3] << std::endl;
        
    }
    
    return 0;
    
}


// Call the acoustic rate //////////////////////////////

// Revised acoustic rate -- buggy
int acoustic_rate_2(std::string out_file, std::string out_file_2) {
    
    // Function has been heavily altered -- refer to original source
    //      In particular, amd vs. am[i]
    
    std::ofstream outfile(out_file.c_str());
    std::ofstream outfile2(out_file_2.c_str());
    
    double c_l, p_const, acoustic, kv, qmax, sum, sum2;
    double deltaq, qvec, minv;
    
    //double fmass = pow((rel_mass_gamma*rel_mass_L*rel_mass_X), (1.0/3.0))*am0;
    
    c_l = density*sound_velocity*sound_velocity;
    p_const = sqrt(2.)*kb*tem/pi/h/c_l*(amd/h)*(sqrt(amd)/h*sqrt(q))*(qh*q)*sigma*sigma;
    
    ///////////////////////////////
    // Begin Absorption Scattering
    ////////////////////////////////
    
    i_count = i_count + 1;
    
    outfile << "energy    acoustic" << std::endl;
    outfile2 << "energy    acoustic" << std::endl;
    
    int i, j, jmax;
    
    for (i = 1; i <= n_lev; i++) {
        ee = de*float(i);
        
        kv = sqrt(ee)*sqrt((amd/h)*qh);
        qmax = 2.0*kv*(amd*sound_velocity/h/kv+1.0);
        
        sum = 0;
        jmax = 100;
        j = 1;
        
        while (j < jmax) {
            
            deltaq = qmax/jmax;
            qvec = float(j)*deltaq;
            sum2 = pow(qvec, 2.0)*deltaq/(exp(h*sound_velocity*qvec/kb/tem)-1.0);
            sum = sum + sum2;
            
            j++;
            
        }
        
        minv = 2*kv*(amd*sound_velocity/h/kv-1);
        
        acoustic = (sum*pow(6.55, 2.0)*amd*pow(qh, 2.0)/pi/density/sound_velocity/kv)-minv;
        scatt_table[i][i_count][iv] = acoustic;
        
        outfile << ee << " " << acoustic << std::endl;
        outfile2 << ee << " " << acoustic << std::endl;
    }
    
    outfile.close();
    outfile2.close();
    
    
    flag_mech[i_count][iv] = 1;
    w[i_count][iv] = 0.;
    i_valley[i_count][iv] = iv;
    
    ////////////////////////////////
    // Begin Emission Scattering
    ////////////////////////////////
    
    i_count = i_count + 1;
    
    // outfile2 << "energy    acoustic" << std::endl;
    
    for (i = 1; i <= n_lev; i++) {
        
        ee = de*float(i);
        
        kv = sqrt(ee)*sqrt(2*(am[iv]/h)*qh);
        qmax = 2*kv*(1-am[iv]*sound_velocity/h/kv);
        
        sum = 0;
        jmax = 100;
        
        j = 1;
        
        while (j < jmax) {
            
            deltaq = qmax/jmax;
            qvec = float(j)*deltaq;
            sum2 = pow(qvec, 2.0)*deltaq/(1.0-exp(-h*sound_velocity*qvec/kb/tem));
            sum = sum + sum2;
            
            j++;
            
        }
        
        acoustic = (sum*pow(6.55, 2.0)*am[iv]*pow(qh, 2.0)/pi/density/sound_velocity/kv);
        scatt_table[i][i_count][iv] = acoustic;
        
        outfile2 << ee << " " << acoustic << std::endl;
    }
    
    outfile2.close();
    
    
    flag_mech[i_count][iv] = 1;
    w[i_count][iv] = 0.;
    i_valley[i_count][iv] = iv;
    
    
    return 0;
}

// General acoustic rate
int acoustic_rate(std::string out_file, std::string out_file_2) {
    
    double c_l, p_const, acoustic;
    
    std::ofstream outfile(out_file.c_str());
    std::ofstream outfile2(out_file_2.c_str());
    
    // Calculate constant
    
    c_l = density*sound_velocity*sound_velocity;
    p_const = sqrt(2.)*kb*tem/pi/h/c_l*(am[iv]/h)*(sqrt(am[iv])/h*sqrt(q))*(qh*q)*sigma*sigma;
    
    // Create scattering table
    
    i_count = i_count + 1;
    
    outfile << "energy    acoustic" << std::endl;
    
    int i;
    for (i = 1; i<=n_lev;i++) {
        ee = de*float(i);
        fe = ee*(1.*af[iv]*ee);
        acoustic = p_const*sqrt(fe)*(1.*af2[iv]*ee);
        scatt_table[i][i_count][iv] = acoustic;
        outfile << ee << " " << acoustic << std::endl;
    }
    
    flag_mech[i_count][iv] = 1;
    w[i_count][iv] = 0.;
    i_valley[i_count][iv] = iv;
    
    outfile.close();
    outfile2.close();
    
    return 0;
}

// Coulomb Scattering //////////////////////////////

int Coulomb_BH(std::string out_file) {
    
    double Debye_length, final_mass, factor, p_const;
    double scatt_rate, ge;
    
    // Calculate constants
    Debye_length = sqrt(eps_sc*Vt/q/doping_density[i_region]);
    Energy_debye[i_region] = hhm[iv]/Debye_length/Debye_length;
    final_mass = am[iv];
    factor = Debye_length*Debye_length/eps_high;
    p_const = doping_density[i_region]*final_mass*qh*sqrt(2.*final_mass)/pi*qh*factor*qh*factor*qh*sqrt(q);
    
    // Calculate scattering rate:
    
    i_count = i_count + 1;
    
    std::ofstream outfile(out_file.c_str());
    
    outfile << "energy    coulomb" << std::endl;
    
    int i;
    for (i = 1; i <= n_lev; i++) {
        ee = de*float(i);
        ge = ee*(1.+af[iv]*ee);
        factor = sqrt(ge)*(1.+af2[iv]*ee)/(1.+4.*ge/Energy_debye[i_region]);
        scatt_rate = p_const*factor;
        scatt_table[i][i_count][iv] = scatt_rate;
        outfile << ee << " " << scatt_rate << std::endl;
    }
    
    outfile.close();
    
    flag_mech[i_count][iv] = 3;
    w[i_count][iv] = 0.0;
    i_valley[i_count][iv] = iv;
    
    
    return 0;
}

// Polar Rate //////////////////////////////

int polar_rate(std::string out_file_1, std::string out_file_2) {
    std::ofstream outfile(out_file_1.c_str());
    std::ofstream outfile2(out_file_2.c_str());
    
    double rnq, p_const, polar_ab, polar_em, A, B, C;
    double ef, ge, gf, rnum, denom, absorption, emission, factor;
    
    // Calculate constant
    
    rnq = 1.0/(exp(w0/Vt)-1.0);
    p_const = qh*qh*q*w0*sqrt(am[iv]/2./q)/4./pi* (1./eps_high - 1./eps_low);
    
    // (a) Scattering rate - absorption
    
    i_count = i_count + 1;
    
    outfile << "energy     polar" << std::endl;
    polar_ab = rnq*p_const;
    
    int i;
    
    for (i = 1; i <= n_lev; i++) {
        
        ee = de*float(i);
        ef = ee + w0;
        ge = ee*(1.+af[iv]*ee);
        gf = ef*(1.+af[iv]*ef);
        rnum = sqrt(ge) + sqrt(gf);
        denom = sqrt(ge) - sqrt(gf);
        A = pow((2*(1.+af2[iv]*ee)*(1.+af[iv]*ef) + af[iv]*(ge+gf)),2);
        B = - af2[iv]*sqrt(ge*gf) * (4.*(1.+af[iv]*ee)*(1.+af[iv]*ef) + af[iv]*(ge+gf));
        C = 4.0*(1.+af[iv]*ee)*(1.+af[iv]*ef)*(1.+af2[iv]*ee)*(1.+af2[iv]*ef);
        A = 4.0;
        C = 4.0;
        B = 0.0;
        factor = (1.+af2[iv]*ef)/sqrt(ge)*(A*log(fabs(rnum/denom))+B)/C;
        absorption = polar_ab*factor;
        scatt_table[i][i_count][iv] = absorption;
        outfile << ee << " " << absorption << std::endl;
        
    }
    
    outfile.close();
    
    flag_mech[i_count][iv] = 2;
    w[i_count][iv] = w0;
    i_valley[i_count][iv] = iv;
    
    // (b) Scattering rate - emission
    
    i_count = i_count + 1;
    
    outfile2 << "energy     polar" << endl;
    
    polar_em = (1.0+rnq)*p_const;
    
    for (i = 1; i <= n_lev; i++) {
        
        ee = de*float(i);
        ef = ee - w0;
        
        if (ef <= 0) {
            emission = 0;
        }
        else {
            ge = ee*(1.+af[iv]*ee);
            gf = ef*(1.+af[iv]*ef);
            rnum = sqrt(ge) + sqrt(gf);
            denom = sqrt(ge) - sqrt(gf);
            A = pow((2*(1.+af2[iv]*ee)*(1.+af[iv]*ef)+af[iv]*(ge+gf)),2);
            B = - af2[iv]*sqrt(ge*gf)*(4.*(1.+af[iv]*ee)*(1.+af[iv]*ef)+af[iv]*(ge+gf));
            C = 4.*(1.+af[iv]*ee)*(1.+af[iv]*ef)*(1.+af2[iv]*ee)*(1.+af2[iv]*ef);
            A = 4.;
            C = 4.;
            B = 0.;
            factor = (1.+af2[iv]*ef)/sqrt(ge)*(A*log(fabs(rnum/denom))+B)/C;
            emission = polar_em*factor;
        }
        
        scatt_table[i][i_count][iv] = emission;
        outfile2 << ee << " " << emission << std::endl;
        
    }
    
    outfile2.close();
    
    flag_mech[i_count][iv] = 2;
    w[i_count][iv] = - w0;
    i_valley[i_count][iv] = iv;
    
    return 0;
}

// Intervalley //////////////////////////////

int intervalley(double w0, std::string out_file_1, std::string out_file_2) {
    std::ofstream outfile(out_file_1.c_str());
    std::ofstream outfile2(out_file_2.c_str());
    
    // Calculate constants
    
    double rnq, final_mass, p_const, ab, em, factor;
    double ef, gf, absorption, emission;
    
    rnq = 1./(exp(w0/Vt)-1.);
    final_mass = am[i_final];
    p_const = final_valleys*(pow(coupling_constant,2))*q*sqrt(q)/(sqrt(2.)*pi*density*w0)*(final_mass/h)*sqrt(final_mass)/h;
    
    // (a) Scattering rate - absorption
    
    i_count = i_count + 1;
    
    outfile << "energy    absorption" << std::endl;
    
    ab = rnq*p_const;
    
    int i;
    for (i = 1; i <= n_lev; i++) {
        
        ee = de*float(i);
        ef = ee + w0 - delta_fi;
        gf = ef*(1.+af[i_final]*ef);
        
        if (ef <= 0) {
            absorption = 0.;
        } else {
            factor = sqrt(gf)*(1.+af2[i_final]*ef);
            absorption = ab*factor;
        }
        
        scatt_table[i][i_count][iv] = absorption;
        outfile << ee << " " << absorption << std::endl;
        
    }
    
    outfile.close();
    
    flag_mech[i_count][iv] = 1;
    w[i_count][iv] = w0 - delta_fi;
    i_valley[i_count][iv] = i_final;
    
    // (b) Scattering rate - emission
    
    i_count = i_count + 1;
    
    outfile2 << "energy     emission" << std::endl;
    
    em = (1.+rnq)*p_const;
    
    for (i = 1; i <= n_lev; i++) {
        
        ee = de*float(i);
        ef = ee - w0 - delta_fi;
        gf = ef*(1.+af[i_final]*ef);
        
        if (ef <= 0) {
            emission = 0.;
        } else {
            factor = sqrt(gf)*(1.+af2[i_final]*ef);
            emission = em*factor;
        }
        
        scatt_table[i][i_count][iv] = emission;
        outfile2 << ee << " " << emission << std::endl;
        
    }
    
    outfile2.close();
    
    flag_mech[i_count][iv] = 1;
    w[i_count][iv] = - w0 - delta_fi;
    i_valley[i_count][iv] = i_final;
    
    return 0;
}

// Re-normalize the table //////////////////////////////

int renormalize_table() {
    
    int i, ik, i_max;
    double tau;
    
    max_scatt_mech[iv] = i_count;
    
    if (max_scatt_mech[iv] >= 1) {
        if (max_scatt_mech[iv] > 1) {
            
            for (i = 2; i <= max_scatt_mech[iv]; i++) {
                for (ik = 1; ik <= n_lev; ik++) {
                    scatt_table[ik][i][iv] = scatt_table[ik][i-1][iv] + scatt_table[ik][i][iv];
                }
            }
            
        }
        
        
        
        // Variables may have wrong type (real -> integer)
        
        i_max = max_scatt_mech[iv];
        tau = 0.;
        
        for (i = 1; i <= n_lev; i++) {
            if (scatt_table[i][i_max][iv] > tau) {
                tau = scatt_table[i][i_max][iv];
            }
        }
        
        for (i = 1; i <= max_scatt_mech[iv]; i++) {
            for (ik = 1; ik <= n_lev; ik++) {
                scatt_table[ik][i][iv] = scatt_table[ik][i][iv]/tau;
            }
        }
        
        tau_max[iv] = 1./tau;
        
        cout << "   valley index = " << iv << "\n   tau_max = " << tau_max[iv] << "\n   tau = " << tau << " ";
    }
    
    return 0;
}

// Scattering table //////////////////////////////

int scat_table() {
    
    std::string table_r;
    
    //////////////////////////////////////////
    //          G A M M A  V A L L E Y
    //
    //////////////////////////////////////////
    
    iv = 1;
    i_count = 0;
    
    // Acoustic phonons scattering rate
    
    if (acoustic_gamma == 1) {
        sigma = sigma_gamma;
        acoustic_rate(working_dir + "gamma/acoustic_ab",working_dir + "gamma/acoustic_em");
    }
    
    // Coulomb scattering rate - Brooks-Herring approach
    
    if (Coulomb_scattering == 1) {
        Coulomb_BH(working_dir + "gamma/coulomb");
    }
    
    // Polar optical phonons scattering rate
    
    if (polar_gamma == 1) {
        w0 = polar_en_gamma;
        polar_rate(working_dir + "gamma/polar_ab",working_dir + "gamma/polar_em");
    }
    
    // Intervalley scattering: gamma to L valley
    
    if (intervalley_gamma_L == 1) {
        w0 = phonon_gamma_L;
        coupling_constant = DefPot_gamma_L;
        delta_fi = split_L_gamma;
        final_valleys = eq_valleys_L;
        i_final = 2;
        intervalley(w0,working_dir + "gamma/intervalley_gamma_L_ab",working_dir + "gamma/intervalley_gamma_L_em");
    }
    
    // Intervalley scattering: gamma to X valley
    
    if (intervalley_gamma_X == 1) {
        w0 = phonon_gamma_X;
        coupling_constant = DefPot_gamma_X;
        delta_fi = split_X_gamma;
        final_valleys = eq_valleys_X;
        i_final = 3;
        intervalley(w0,working_dir + "gamma/intervalley_gamma_X_ab",working_dir + "gamma/intervalley_gamma_X_em");
    }
    
    std::cout << "\n\nMechanisms in the gamma valley = " << i_count << "\n\n";
    renormalize_table();
    
    if (i_count > 0) {
        
        table_r = working_dir + "gamma/gamma_table_renormalized";
        std::ofstream g_file(table_r.c_str());
        
        int i;
        for (i = 1; i<=n_lev; i++) {
            ee = float(i)*de;
            
            g_file << ee << " " << scatt_table[i][1][1] << " " << scatt_table[i][2][1] << " " << scatt_table[i][3][1] << " " << scatt_table[i][4][1] << " " << scatt_table[i][5][1] << " " << scatt_table[i][6][1] << " " << scatt_table[i][7][1] << " " << scatt_table[i][8][1] << " " << scatt_table[i][9][1] << " " << scatt_table[i][10][1] << std::endl;
            
        }
        
        g_file.close();
    }
    
    //////////////////////////////////////////
    //          L  V A L L E Y
    //
    //////////////////////////////////////////
    
    
    iv = 2;
    i_count = 0;
    
    // Acoustic phonons scattering rate
    
    if (acoustic_L == 1) {
        sigma = sigma_L;
        acoustic_rate(working_dir + "L/acoustic_ab",working_dir + "L/acoustic_em");
    }
    
    // Coulomb scattering rate - Brooks-Herring approach
    
    if (Coulomb_scattering == 1) {
        Coulomb_BH(working_dir + "L/coulomb");
    }
    
    // Polar optical phonons scattering rate
    
    if (polar_L == 1) {
        w0 = polar_en_L;
        polar_rate(working_dir + "L/polar_ab",working_dir + "L/polar_em");
    }
    
    // Intervalley scattering: gamma to L valley
    
    if (intervalley_L_gamma == 1) {
        w0 = phonon_L_gamma;
        coupling_constant = DefPot_L_gamma;
        delta_fi = split_L_gamma;
        final_valleys = eq_valleys_gamma;
        i_final = 1;
        intervalley(w0,working_dir + "L/intervalley_L_gamma_ab",working_dir + "L/intervalley_L_gamma_em");
    }
    
    // Intervalley scattering: gamma to X valley
    
    if (intervalley_L_L == 1) {
        w0 = phonon_L_L;
        coupling_constant = DefPot_L_L;
        delta_fi = 0.;
        final_valleys = eq_valleys_L - 1;
        i_final = 2;
        intervalley(w0,working_dir + "L/intervalley_L_L_ab",working_dir + "L/intervalley_L_L_em");
    }
    
    
    // Intervalley scattering: gamma to X valley
    
    if (intervalley_L_X == 1) {
        w0 = phonon_L_X;
        coupling_constant = DefPot_L_X;
        delta_fi = split_X_gamma - split_L_gamma;
        final_valleys = eq_valleys_X;
        i_final = 3;
        intervalley(w0,working_dir + "L/intervalley_L_X_ab",working_dir + "L/intervalley_L_X_em");
    }
    
    std::cout << "\n\nMechanisms in the L valley = " << i_count << "\n\n";
    renormalize_table();
    
    if (i_count > 0) {
        table_r = working_dir + "L/L_table_renormalized";
        std::ofstream l_file(table_r.c_str());
        
        int i;
        for (i = 1; i<=n_lev; i++) {
            ee = float(i)*de;
            
            l_file << ee << " " << scatt_table[i][1][2] << " " << scatt_table[i][2][2] << " " << scatt_table[i][3][2] << " " << scatt_table[i][4][2] << " " << scatt_table[i][5][2] << " " << scatt_table[i][6][2] << " " << scatt_table[i][7][2] << " " << scatt_table[i][8][2] << " " << scatt_table[i][9][2] << " " << scatt_table[i][10][2] << std::endl;
            
        }
        
        l_file.close();
    }
    
    //////////////////////////////////////////
    //          X  V A L L E Y
    //
    //////////////////////////////////////////
    
    
    iv = 3;
    i_count = 0;
    
    // Acoustic phonons scattering rate
    
    if (acoustic_X == 1) {
        sigma = sigma_X;
        acoustic_rate(working_dir + "X/acoustic_ab",working_dir + "X/acoustic_em");
    }
    
    // Coulomb scattering rate - Brooks-Herring approach
    
    if (Coulomb_scattering == 1) {
        Coulomb_BH(working_dir + "X/coulomb");
    }
    
    // Polar optical phonons scattering rate
    
    if (polar_X == 1) {
        w0 = polar_en_X;
        polar_rate(working_dir + "X/polar_ab",working_dir + "X/polar_em");
    }
    
    // Intervalley scattering: gamma to L valley
    
    if (intervalley_X_gamma == 1) {
        w0 = phonon_X_gamma;
        coupling_constant = DefPot_X_gamma;
        delta_fi = - split_X_gamma;
        final_valleys = eq_valleys_gamma;
        i_final = 1;
        intervalley(w0,working_dir + "X/intervalley_X_gamma_ab",working_dir + "X/intervalley_X_gamma_em");
    }
    
    // Intervalley scattering: gamma to X valley
    
    if (intervalley_X_L == 1) {
        w0 = phonon_X_L;
        coupling_constant = DefPot_X_L;
        delta_fi = split_L_gamma - split_X_gamma;
        final_valleys = eq_valleys_L;
        i_final = 2;
        intervalley(w0,working_dir + "X/intervalley_X_L_ab",working_dir + "X/intervalley_X_L_ab");
    }
    
    
    // Intervalley scattering: gamma to X valley
    
    if (intervalley_X_X == 1) {
        w0 = phonon_X_X;
        coupling_constant = DefPot_X_X;
        delta_fi = 0.;
        final_valleys = eq_valleys_X - 1.;
        i_final = 3;
        intervalley(w0,working_dir + "X/intervalley_X_X_ab",working_dir + "X/intervalley_X_X_em");
    }
    
    std::cout << "\n\nMechanisms in the X valley = " << i_count << "\n\n";
    renormalize_table();
    
    if (i_count > 0) {
        
        table_r = working_dir + "X/X_table_renormalized";
        std::ofstream x_file(table_r.c_str());
        
        int i;
        for (i = 1; i<=n_lev; i++) {
            ee = float(i)*de;
            
            x_file << ee << " " << scatt_table[i][1][3] << " " << scatt_table[i][2][3] << " " << scatt_table[i][3][3] << " " << scatt_table[i][4][3] << " " << scatt_table[i][5][3] << " " << scatt_table[i][6][3] << " " << scatt_table[i][7][3] << " " << scatt_table[i][8][3] << " " << scatt_table[i][9][3] << " " << scatt_table[i][10][3] << std::endl;
            
        }
        
        x_file.close();
    }
    
    return 0;
}
